/*
 * SFF8472_AW.c
 *
 *  Created on: 2017�~3��9��
 *      Author: 100476
 */
#include <SI_EFM8LB1_Register_Enums.h>                  // SFR declarations
#include <MALD37645A_SRAM.h>
#include <MATA37644A_SRAM.h>
#include <Master_I2C_GPIO.h>
#include "MSA_SFF8472.h"
#include "Calibration_Struct.h"


SI_SBIT( INTERRUPT , SFR_P1, 5);
SI_SBIT( TX_DISABLE , SFR_P1, 1);

SI_SBIT( RS0_MCU , SFR_P0, 5 );
SI_SBIT( RS1_MCU , SFR_P0, 4 );

extern xdata unsigned char SFF8472_A2[256];
xdata unsigned char TXD_SW_FLAG = 0 ;
xdata unsigned char bef_INTERRUPT;

void Temperature_Alarm_warning()
{
    int T_Temp,H_Alarm,L_Alarm,H_Warn,L_Warn;

    T_Temp  = (unsigned int)SFF8472_A2[96]<<8 | SFF8472_A2[97];
    H_Alarm = (unsigned int)SFF8472_A2[0] <<8 | SFF8472_A2[1] ;
    L_Alarm = (unsigned int)SFF8472_A2[2] <<8 | SFF8472_A2[3] ;
    H_Warn  = (unsigned int)SFF8472_A2[4] <<8 | SFF8472_A2[5] ;
    L_Warn  = (unsigned int)SFF8472_A2[6] <<8 | SFF8472_A2[7] ;

	if (T_Temp >  H_Alarm)
		SFF8472_A2[112] |= 0x80;		// set high alarm flag bit7
	else
		SFF8472_A2[112] &= ~0x80;		// clear high alarm flag bit7

    if (T_Temp >  H_Warn)
    	SFF8472_A2[116] |= 0x80;		// set high warning flag bit7
    else
    	SFF8472_A2[116] &= ~0x80;		// clear high warning flag bit7

    if (T_Temp <  L_Warn)
    	SFF8472_A2[116] |= 0x40;		// set low warning flag bit6
    else
		SFF8472_A2[116] &= ~0x40;		// clear low warning flag bit6

    if (T_Temp <  L_Alarm)
    	SFF8472_A2[112] |= 0x40;		// set low alarm flag bit6
    else
		SFF8472_A2[112] &= ~0x40;		// clear low alarm flag bit6

}

void VCC_Alarm_warning()
{
	unsigned int VCC_T,H_Alarm,L_Alarm,H_Warn,L_Warn;

	VCC_T   = (unsigned int)SFF8472_A2[98]<<8  | SFF8472_A2[99];
    H_Alarm = (unsigned int)SFF8472_A2[ 8]<<8  | SFF8472_A2[9] ;
    L_Alarm = (unsigned int)SFF8472_A2[10]<<8  | SFF8472_A2[11] ;
    H_Warn  = (unsigned int)SFF8472_A2[12]<<8  | SFF8472_A2[13] ;
    L_Warn  = (unsigned int)SFF8472_A2[14]<<8  | SFF8472_A2[15] ;


	if (VCC_T >  H_Alarm)
		SFF8472_A2[112] |= 0x20;		// set high alarm flag bit5
	else
		SFF8472_A2[112] &= ~0x20;

    if (VCC_T >  H_Warn)
    	SFF8472_A2[116] |= 0x20;		// set high warning flag bit5
    else
    	SFF8472_A2[116] &= ~0x20;

    if (VCC_T <  L_Warn)
    	SFF8472_A2[116] |= 0x10;		// set low warning flag bit4
    else
		SFF8472_A2[116] &= ~0x10;

    if (VCC_T <  L_Alarm)
    	SFF8472_A2[112] |= 0x10;		// set low alarm flag bit4
    else
		SFF8472_A2[112] &= ~0x10;



}

void Bias_Alarm_warning()
{
	unsigned int Bias_T,H_Alarm,L_Alarm,H_Warn,L_Warn;

	Bias_T  = (unsigned int)SFF8472_A2[100]<<8 | SFF8472_A2[101];
    H_Alarm = (unsigned int)SFF8472_A2[16] <<8 | SFF8472_A2[17] ;
    L_Alarm = (unsigned int)SFF8472_A2[18] <<8 | SFF8472_A2[19] ;
    H_Warn  = (unsigned int)SFF8472_A2[20] <<8 | SFF8472_A2[21] ;
    L_Warn  = (unsigned int)SFF8472_A2[22] <<8 | SFF8472_A2[23] ;


	if (Bias_T >  H_Alarm)
		SFF8472_A2[112] |= 0x08;		// set high alarm flag bit3
	else
		SFF8472_A2[112] &= ~0x08;

    if (Bias_T >  H_Warn)
    	SFF8472_A2[116] |= 0x08;		// set high warning flag bit3
    else
    	SFF8472_A2[116] &= ~0x08;

    if (Bias_T <  L_Warn)
    	SFF8472_A2[116] |= 0x04;		// set low warning flag bit2
    else
		SFF8472_A2[116] &= ~0x04;

    if (Bias_T <  L_Alarm)
    	SFF8472_A2[112] |= 0x04;		// set low alarm flag bit2
    else
		SFF8472_A2[112] &= ~0x04;

}

void TxPower_Alarm_warning()
{
	unsigned int TxPower_T,H_Alarm,L_Alarm,H_Warn,L_Warn;

	TxPower_T  = (unsigned int)SFF8472_A2[102]<<8 | SFF8472_A2[103];
    H_Alarm =    (unsigned int)SFF8472_A2[24] <<8 | SFF8472_A2[25] ;
    L_Alarm =    (unsigned int)SFF8472_A2[26] <<8 | SFF8472_A2[27] ;
    H_Warn  =    (unsigned int)SFF8472_A2[28] <<8 | SFF8472_A2[29] ;
    L_Warn  =    (unsigned int)SFF8472_A2[30] <<8 | SFF8472_A2[31] ;



	if (TxPower_T >  H_Alarm)
		SFF8472_A2[112] |= 0x02;		// set high alarm flag bit1
	else
		SFF8472_A2[112] &= ~0x02;

    if (TxPower_T >  H_Warn)
    	SFF8472_A2[116] |= 0x02;		// set high warning flag bit3
    else
    	SFF8472_A2[116] &= ~0x02;

    if (TxPower_T <  L_Warn)
	    SFF8472_A2[116] |= 0x01;		// set low warning flag bit0
    else
		SFF8472_A2[116] &= ~0x01;

    if (TxPower_T <  L_Alarm)
    	SFF8472_A2[112] |= 0x01;		// set low alarm flag bit0
    else
		SFF8472_A2[112] &= ~0x01;


}

void RxPower_Alarm_warning()
{
	unsigned int RxPower_T,H_Alarm,L_Alarm,H_Warn,L_Warn;

	RxPower_T  = (unsigned int)SFF8472_A2[104]<<8 | SFF8472_A2[105];
    H_Alarm    = (unsigned int)SFF8472_A2[32] <<8 | SFF8472_A2[33] ;
    L_Alarm    = (unsigned int)SFF8472_A2[34] <<8 | SFF8472_A2[35] ;
    H_Warn     = (unsigned int)SFF8472_A2[36] <<8 | SFF8472_A2[37] ;
    L_Warn     = (unsigned int)SFF8472_A2[38] <<8 | SFF8472_A2[39] ;

	if (RxPower_T >  H_Alarm)
		SFF8472_A2[113] |= 0x80;		// set high alarm flag bit7
	else
		SFF8472_A2[113] &= ~0x80;

	if (RxPower_T >  H_Warn)
		SFF8472_A2[117] |= 0x80;		// set high warning flag bit7
	else
		SFF8472_A2[117] &= ~0x80;

	if (RxPower_T <  L_Warn)
		SFF8472_A2[117] |= 0x40;		// set low warning flag bit6
	else
		SFF8472_A2[117] &= ~0x40;

	if (RxPower_T <  L_Alarm)
		SFF8472_A2[113] |= 0x40;		// set low alarm flag bit6
	else
		SFF8472_A2[113] &= ~0x40;
}

void Optional_Status()
{
	// A2h[110]
	// bit 7 Tx_Disable
	// bit 6 Soft_Tx Disable select
	// bit 5 RS1_State
	// bit 4 RS0_State
	// bit 3 Soft Rate_Select
	// bit 2 TX Fault State
	// bit 1 Rx_LOS State
	// bit 0 Data_Ready_Bar State

	if( (INTERRUPT==1 ) && ( INTERRUPT!=bef_INTERRUPT ))
	{
		MALD37645A_MEMORY_MAP.LOS_LOL_Fault_Status = Macom_ReadByte( MALD37645A_SA , 0x0B ) ;

		if(MALD37645A_MEMORY_MAP.LOS_LOL_Fault_Status&0xE0)
			SFF8472_A2[110]=(SFF8472_A2[110]|0x04);
		else
			SFF8472_A2[110]=(SFF8472_A2[110]&0xFB);
		bef_INTERRUPT = INTERRUPT ;
	}
	else
		SFF8472_A2[110]=(SFF8472_A2[110]&0xFB);

	if( TX_DISABLE == 1 )
		SFF8472_A2[110]=(SFF8472_A2[110]|0x80);
	else
		SFF8472_A2[110]=(SFF8472_A2[110]&0x7F);

	// TxD_SW
	if( ( SFF8472_A2[110] & 0x40 ) == 0x40 )
		TXD_SW_FLAG = 1;
	else
		TXD_SW_FLAG = 0;

	// RS0 State
	if(RS0_MCU==1)
		SFF8472_A2[110] |= 0x10;
	else
		SFF8472_A2[110] &= ~0x10;
	// RS1 State
	if(RS1_MCU==1)
		SFF8472_A2[110] |= 0x20;
	else
		SFF8472_A2[110] &= ~0x20;



}

void CDR_LOL_Status(unsigned char TRX_CDR_Status)
{
	MALD37645A_MEMORY_MAP.LOS_LOL_Fault_Status = Macom_ReadByte( MALD37645A_SA , 0x0B ) ;
	MATA37644A_MEMORY_MAP.LOS_LOL_STATUS       = Macom_ReadByte( MATA37644A_SA , 0x0B ) ;
	// TX CDR LOL Status
	if( ( TRX_CDR_Status == 1 ) )
	{
		if(MALD37645A_MEMORY_MAP.LOS_LOL_Fault_Status & 0x02)
			SFF8472_A2[119] |= 0x02 ;
		else
			SFF8472_A2[119] &= ~0x02 ;
	}
	// RX CDR LOL Status
	if( ( TRX_CDR_Status == 2 ) | ( TRX_CDR_Status == 1 ) )
	{
		if(MATA37644A_MEMORY_MAP.LOS_LOL_STATUS & 0x02)
			SFF8472_A2[119] |= 0x01 ;
		else
			SFF8472_A2[119] &= ~0x01 ;
	}
}


void SFF8472_AW_Function()
{
	Temperature_Alarm_warning();
	VCC_Alarm_warning();
	Bias_Alarm_warning();
	TxPower_Alarm_warning();
	RxPower_Alarm_warning();
	Optional_Status();
	CDR_LOL_Status(1);
}




