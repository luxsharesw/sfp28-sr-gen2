/*
 * SFF8472_DDMI.c
 *
 *  Created on: 2017�~3��9��
 *      Author: 100476
 */
#include <SI_EFM8LB1_Register_Enums.h>
#include <MALD37645A_SRAM.h>
#include <MATA37644A_SRAM.h>
#include "Calibration_Struct.h"
#include "MSA_SFF8472.h"
#include <Master_I2C_GPIO.h>

#define SHOW_RSSI_Enable   1
#define SHOW_RSSI_Disable  0

xdata unsigned char SHOW_RSSI = 1 ;

xdata char Temperature_Indx = 0 ;
SI_SBIT( RX_LOS , SFR_P2, 1);

#define MIN_TEMP    -40

xdata unsigned char PrevTindex = 0 ;
// DAC 12bit output function

/*
unsigned int Get_TempIndex()
{
	int TempBuffer ;
	unsigned int CurrTIndex , Flash_Address;

	TempBuffer = ( Internel_Tempature/256 ) ;

	CurrTIndex = ( TempBuffer - MIN_TEMP )>>1 ;

	if(CurrTIndex<PrevTindex)
		CurrTIndex = ( TempBuffer - MIN_TEMP + 1)>>1 ;

	PrevTindex = CurrTIndex ;

	Flash_Address = CurrTIndex * 2 ;

	return Flash_Address ;

}
*/

int Temp_CAL( int Vaule , unsigned char Slop , unsigned char Shift , int Offset)
{
	long Buffer_temp ;

	Buffer_temp = (long)Vaule * Slop ;
	Buffer_temp = (long)Buffer_temp >> Shift ;

	Vaule = (long)Buffer_temp + Offset * 256 ;

    return Vaule ;
}

unsigned int CAL_Function( unsigned int Vaule , unsigned char Slop , unsigned char Shift , int Offset )
{
	long Buffer_temp ;

	Buffer_temp = (long)Vaule * Slop ;
	Buffer_temp = (long)Buffer_temp >> Shift ;

	Vaule = (long)Buffer_temp + Offset;

	return Vaule;
}

void Temperature_Monitor(int tempsensor_value)
{
	int Temp_Buffer;
	char Indx_Temp ;
	Temp_Buffer = Temp_CAL(tempsensor_value,CALIB_MEMORY_MAP.TEMP_SCALE1M,CALIB_MEMORY_MAP.TEMP_SCALE1L,CALIB_MEMORY_MAP.TEMP_OFFSET1);

	SFF8472_A2[96] = Temp_Buffer >> 8 ;
	SFF8472_A2[97] = Temp_Buffer ;

	Indx_Temp = SFF8472_A2[96];

	if(Indx_Temp<-40)
		Indx_Temp = -40;
	if(Indx_Temp>=85)
		Indx_Temp = 85 ;

	Temperature_Indx = ( Indx_Temp+40 )/2 ;
	if(Temperature_Indx > 63 ) Temperature_Indx = 63;
}

void VCC_Monitor(unsigned int VCC_Vaule)
{
	VCC_Vaule = CAL_Function( VCC_Vaule , CALIB_MEMORY_MAP.VCC_SCALEM , CALIB_MEMORY_MAP.VCC_SCALEL , CALIB_MEMORY_MAP.VCC_OFFSET ) ;

    SFF8472_A2[98] = VCC_Vaule >> 8 ;
	SFF8472_A2[99] = VCC_Vaule ;
}

void BiasCurrent_Monitor(unsigned int BiasCurrent_Value)
{
	// SFF842 Status Tx Disable
    if( SFF8472_A2[110] & 0xC4 )
	{
	    SFF8472_A2[100] = 0x00 ;
		SFF8472_A2[101] = 0x00 ;
	}
	else
	{
		BiasCurrent_Value = CAL_Function( BiasCurrent_Value , CALIB_MEMORY_MAP.IBias0_SCALEM , CALIB_MEMORY_MAP.IBias0_SCALE1L , CALIB_MEMORY_MAP.IBias0_OFFSET ) ;

    	SFF8472_A2[100] = ( BiasCurrent_Value >> 8 ) ;
		SFF8472_A2[101] = BiasCurrent_Value ;
	}
}

void TxPower_Monitor(unsigned int TxPower_Vaule)
{
	// SFF842 Status Tx Disable
    if( SFF8472_A2[110] & 0xC4 )
	{
	    SFF8472_A2[102] = 0 ;
		SFF8472_A2[103] = 0 ;
	}
	else
	{
		TxPower_Vaule = CAL_Function( TxPower_Vaule , CALIB_MEMORY_MAP.TXP0_SCALEM , CALIB_MEMORY_MAP.TXP0_SCALEL , CALIB_MEMORY_MAP.TXP0_OFFSET ) ;

	    SFF8472_A2[102] = ( TxPower_Vaule >> 8 ) ;
	    SFF8472_A2[103] = TxPower_Vaule ;
	}
}

void RxPower_DC_Monitor( unsigned int RxPower_Vaule )
{
	unsigned int RSSI_Temp;

	//RSSI_Temp = RxPower_Vaule ;
	RSSI_Temp = CAL_Function( RxPower_Vaule , CALIB_MEMORY_MAP.RX0_SCALEM , CALIB_MEMORY_MAP.RX0_SCALEL, CALIB_MEMORY_MAP.RX0_OFFSET) ;

	if(RSSI_Temp>0xF000)
		RSSI_Temp = 0 ;

	// DC LOS Function
	if( RSSI_Temp < CALIB_MEMORY_MAP.Rx0_LOS_Assret )
	{
		MATA37644A_MEMORY_MAP.OUTPUT_CTRL |= 0x01 ;
		Macom_WriteByte( MATA37644A_SA, 0x20 , MATA37644A_MEMORY_MAP.OUTPUT_CTRL ) ;

		SHOW_RSSI = SHOW_RSSI_Disable;
	}

	if( RSSI_Temp >= CALIB_MEMORY_MAP.Rx0_LOS_DeAssret )
	{
		MATA37644A_MEMORY_MAP.OUTPUT_CTRL &= ~0x01 ;
		Macom_WriteByte( MATA37644A_SA, 0x20 , MATA37644A_MEMORY_MAP.OUTPUT_CTRL ) ;

		SHOW_RSSI = SHOW_RSSI_Enable;
	}

	if( SHOW_RSSI == SHOW_RSSI_Enable )
	{
		SFF8472_A2[110] &= ~0x02 ;
		RX_LOS = 0 ;
		SFF8472_A2[104] = ( RSSI_Temp >> 8 );
		SFF8472_A2[105] = RSSI_Temp ;
	}
	else
	{
		SFF8472_A2[110] |= 0x02 ;
		RX_LOS = 1 ;
		SFF8472_A2[104] = 0 ;
		SFF8472_A2[105] = 1 ;
	}
}

void RxPower_AC_Monitor( unsigned int RxPower_Vaule )
{
	unsigned int RSSI_Temp;

	RSSI_Temp = CAL_Function( RxPower_Vaule , CALIB_MEMORY_MAP.RX1_SCALEM , CALIB_MEMORY_MAP.RX1_SCALEL , CALIB_MEMORY_MAP.RX1_OFFSET ) ;
    // AC LOS Function
	MATA37644A_MEMORY_MAP.LOS_LOL_STATUS = Macom_ReadByte( MATA37644A_SA , 0x0B ) ;

	if( MATA37644A_MEMORY_MAP.LOS_LOL_STATUS & 0x01 == 0x01 )
	{
		MATA37644A_MEMORY_MAP.OUTPUT_CTRL |= 0x01 ;
		Macom_WriteByte( MATA37644A_SA, 0x20 , MATA37644A_MEMORY_MAP.OUTPUT_CTRL ) ;
		SFF8472_A2[110] |= 0x02 ;
		SFF8472_A2[104]=0;
		SFF8472_A2[105]=1;
	}
	else
	{
		MATA37644A_MEMORY_MAP.OUTPUT_CTRL &= ~0x01 ;
		Macom_WriteByte( MATA37644A_SA, 0x20 , MATA37644A_MEMORY_MAP.OUTPUT_CTRL ) ;
		SFF8472_A2[110] &= ~0x02 ;
		SFF8472_A2[104] = ( RSSI_Temp >> 8 );
		SFF8472_A2[105] = RSSI_Temp ;
	}
}


void SFF8472_Monitor()
{
	// Vcc       MSA Base 100uV
	// Ibias     MSA Base 2uA
	// TxB & RxB MSA Base 0.1uW
	unsigned int IB_Current;

	IB_Current = Get_Ibias_Current();

	Temperature_Monitor( Get_Tempsensor());
	VCC_Monitor( VCC_GetValue());
	BiasCurrent_Monitor( IB_Current );
	TxPower_Monitor( IB_Current );

//	CALIB_MEMORY_MAP.DEBUG_TEMP3 = V18_GetValue();

	if(CALIB_MEMORY_MAP.LOS_DC_AC == 0x01)
		RxPower_DC_Monitor( RSSI_GET_Value() );
	else
		RxPower_AC_Monitor( RSSI_GET_Value() );

}











