/*
 * MSA_SFF8472.h
 *
 *  Created on: 2017�~3��10��
 *      Author: 100476
 */

#ifndef INC_MSA_SFF8472_H_
#define INC_MSA_SFF8472_H_

//--------------------------------------------------//
// AW DDMI Function
//--------------------------------------------------//
extern void SFF8472_AW_Function();

//--------------------------------------------------//
// Tempsensor_ADC.c
//--------------------------------------------------//
extern void Tempsonr_0C_internal_Cal();
extern int Get_Tempsensor();
extern unsigned int VCC_GetValue();
extern unsigned int V18_GetValue();
extern unsigned int RSSI_GET_Value();
extern unsigned int Get_Ibias_Current();
//--------------------------------------------------//
// SFF8472_DDMI.c
//--------------------------------------------------//
extern void SFF8472_Monitor();

extern xdata char Temperature_Indx ;
//--------------------------------------------------//
// SFF8472_AW.c
//--------------------------------------------------//
extern void SFF8472_AW_Function();
extern void CDR_LOL_Status(unsigned char TRX_CDR_Status);

extern xdata unsigned char TXD_SW_FLAG;
//--------------------------------------------------//
// MALD37645A.c
//--------------------------------------------------//
extern void CTLE_Control(unsigned char MSA_Vaule,unsigned char HighLow_Rate_Select);
extern void Get_LUT_SET_BIAS_MOD();
extern void TXD_Function();
extern void Tx_CDR_Control( unsigned char  ONOFF_Control);
//--------------------------------------------------//
// MALD37645A.c
//--------------------------------------------------//
extern void OUTPUT_EMPHASIS_Control(unsigned char EMP_Value , unsigned char HighLow_Rate_Select );
extern void RX_CDR_Control( unsigned char  ONOFF_Control);
//--------------------------------------------------//
// Power_On_Init.c
//--------------------------------------------------//
extern void Get_Flash_PW_LEVE1();

extern xdata unsigned char SFF8472_A2[256];
extern xdata unsigned char SFF8472_A0[256];
extern xdata unsigned char EMPHASIS_Table[16];
extern xdata unsigned char CTEL_Table[16];
extern xdata unsigned char PW_LEVE1[4];


#endif /* INC_MSA_SFF8472_H_ */
