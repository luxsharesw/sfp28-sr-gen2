/*
 * Calibration_Struct.h
 *
 *  Created on: 2017/3/8
 *      Author: Ivan_Lin
 */

#ifndef CALIBRATION_STRUCT_H_
#define CALIBRATION_STRUCT_H_

xdata struct CALIB_MEMORY
{
	unsigned char VCC_SCALEM;           //Address = 0x80
	unsigned char VCC_SCALEL;           //Address = 0x81
			 int  VCC_OFFSET;           //Address = 0x82 - 0x83

	unsigned char TEMP_SCALE1M;         //Address = 0x84
	unsigned char TEMP_SCALE1L;         //Address = 0x85
			 char TEMP_OFFSET1;         //Address = 0x86

	unsigned char IBias0_SCALEM;        //Address = 0x87
	unsigned char IBias0_SCALE1L;       //Address = 0x88
		     int  IBias0_OFFSET;        //Address = 0x89 - 0x8A

	unsigned char IBias1_SCALEM;        //Address = 0x8B
	unsigned char IBias1_SCALE1L;       //Address = 0x8C
			 int  IBias1_OFFSET;        //Address = 0x8D - 0x8E

	unsigned char IBias2_SCALEM;        //Address = 0x8F
	unsigned char IBias2_SCALE1L;       //Address = 0x90
			 int  IBias2_OFFSET;        //Address = 0x91 - 0x92

	unsigned char IBias3_SCALEM;        //Address = 0x93
	unsigned char IBias3_SCALE1L;       //Address = 0x94
			 int  IBias3_OFFSET;        //Address = 0x95 - 0x96

	unsigned char TXP0_SCALEM;          //Address = 0x97
	unsigned char TXP0_SCALEL;          //Address = 0x98
	         int  TXP0_OFFSET;          //Address = 0x99 - 0x9A

	unsigned char TXP1_SCALEM;          //Address = 0x9B
	unsigned char TXP1_SCALEL;          //Address = 0x9C
			 int  TXP1_OFFSET;          //Address = 0x9D - 0x9E

	unsigned char TXP2_SCALEM;          //Address = 0x9F
	unsigned char TXP2_SCALEL;          //Address = 0xA0
			 int  TXP2_OFFSET;          //Address = 0xA1 - 0xA2

	unsigned char TXP3_SCALEM;          //Address = 0xA3
	unsigned char TXP3_SCALEL;          //Address = 0xA4
			 int  TXP3_OFFSET;          //Address = 0xA5 - 0xA6

	unsigned char RX0_SCALEM;           //Address = 0xA7
	unsigned char RX0_SCALEL;           //Address = 0xA8
			 int  RX0_OFFSET;           //Address = 0xA9 - 0xAA

	unsigned char RX1_SCALEM;           //Address = 0xAB
	unsigned char RX1_SCALEL;           //Address = 0xAC
			 int  RX1_OFFSET;           //Address = 0xAD - 0xAE

	unsigned char RX2_SCALEM;           //Address = 0xAF
	unsigned char RX2_SCALEL;           //Address = 0xB0
			 int  RX2_OFFSET;           //Address = 0xB1 - 0xB2

	unsigned char RX3_SCALEM;           //Address = 0xB3
	unsigned char RX3_SCALEL;           //Address = 0xB4
			 int  RX3_OFFSET;           //Address = 0xB5 - 0xB6

    unsigned int  Rx0_LOS_Assret;       //Address = 0xB7 - 0xB8
    unsigned int  Rx0_LOS_DeAssret;     //Address = 0xB9 - 0xBA
    unsigned int  Rx1_LOS_Assret;       //Address = 0xBB - 0xBC
    unsigned int  Rx1_LOS_DeAssret;     //Address = 0xBD - 0xBE
    unsigned int  Rx2_LOS_Assret;       //Address = 0xBF - 0xC0
    unsigned int  Rx2_LOS_DeAssret;     //Address = 0xC1 - 0xC2
    unsigned int  Rx3_LOS_Assret;       //Address = 0xC3 - 0xC4
    unsigned int  Rx3_LOS_DeAssret;     //Address = 0xC5 - 0xC6

    unsigned char LUT_High_Temp;        //Address = 0xC7
    unsigned char LUT_Low_Temp;         //Address = 0xC8
    unsigned char LOS_DC_AC;            //Address = 0xC9
    unsigned char RSSI_MODE;            //Address = 0xCA
    unsigned char SOF_MODSEL;           //Address = 0xCB
    unsigned char LUT_EN;               //Address = 0xCC

    unsigned int Tx_Power_Value_CH0;    //Address = 0xCD - 0xCE
             int Tx_power_Cal_CH0;      //Address = 0xCF - 0xD0
    unsigned int Tx_Power_Value_CH1;    //Address = 0xD1 - 0xD2
             int Tx_power_Cal_CH1;      //Address = 0xD3 - 0xD4
    unsigned int Tx_Power_Value_CH2;    //Address = 0xD5 - 0xD6
             int Tx_power_Cal_CH2;      //Address = 0xD7 - 0xD8
    unsigned int Tx_Power_Value_CH3;    //Address = 0xD9 - 0xDA
             int Tx_power_Cal_CH3;      //Address = 0xDB - 0xDC
    unsigned int Tx_Power_HT_CH0;       //Address = 0xDD - 0xDE
    unsigned int Tx_Power_HT_CH1;		//Address = 0xDF - 0xE0
	unsigned int Tx_Power_HT_CH2;       //Address = 0xE1 - 0xE2
    unsigned int Tx_Power_HT_CH3;		//Address = 0xE3 - 0xE4

    unsigned char CAL_Buffer[17];       //Address = 0xE5 - 0xF5

    unsigned char Error_Flag;           //Address = 0xF6
    unsigned char DEBUG_TEMP1;          //Address = 0xF7
    unsigned int  CHECKSUM_V;           //Address = 0xF8 - 0xF9
    unsigned int  FW_VERSION;           //Address = 0xFA - 0xFB
    unsigned int  DEBUG_TEMP2;          //Address = 0xFC - 0xFD
    unsigned int  DEBUG_TEMP3;          //Address = 0xFE - 0xFF

};

extern xdata struct CALIB_MEMORY CALIB_MEMORY_MAP;

#endif /* CALIBRATION_STRUCT_H_ */
