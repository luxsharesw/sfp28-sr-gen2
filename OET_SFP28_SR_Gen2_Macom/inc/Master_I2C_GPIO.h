/*
 * Silabs_GPIO_MI2C.h
 *
 *  Created on: 2016/3/1
 *      Author: Ivan_Lin
 */

#ifndef SILABS_GPIO_MI2C_H_
#define SILABS_GPIO_MI2C_H_

extern void Delay_us(unsigned char time_us);
extern void Delay_1ms(void);
extern void Delay_ms(int time_ms);
extern void Macom_WriteByte(unsigned char Slave_address,unsigned char Regcommand_Address,unsigned char SendData);
extern unsigned char Macom_ReadByte(unsigned char Slave_address,unsigned char Regcommand_Address);
extern unsigned int Macom_Read_Two_Byte(unsigned char Slave_address,unsigned char Regcommand_Address);

#endif /* SILABS_GPIO_MI2C_H_ */
