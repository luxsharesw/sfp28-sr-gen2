/*
 * Flash_MAP.h
 *
 *  Created on: 2017�~3��6��
 *      Author: 100476
 */

#ifndef INC_FLASH_MAP_H_
#define INC_FLASH_MAP_H_

#define MCU_FW_VERSION       0x0301

#define Flash_LUT_Start      0xC800   // Flash 50K

#define FS_SFF8472_A0        0xC800   // 50K
#define FS_SFF8472_A2        0xCA00   // 50.5K
#define FS_SFF8472_P0        0xCA80

#define FS_SFF8472_P1        0xCC00   // 51 K
#define FS_SFF8472_P2        0xCE00   // 51.5 K

#define FS_MALD_37645A       0xD000   // 52K
#define FS_MATA_37644A       0xD200   // 52.5K
#define Calibration_Table    0xD400   // 53 K
#define MSA_Option_Table     0xD600   // 53.5 K

#define Bias_LUT_0           0xD800   // 54 K
#define Mod_LUT_0            0xDA00   // 54.5 K

#define MPLID_INFO           0xF600   // 61.5 K
#define BL_CS_CK             0xF800   // 62 K

#define Flash_End	         0xF9FF


#endif /* INC_FLASH_MAP_H_ */
