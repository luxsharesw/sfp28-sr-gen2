/*
 * MA37045B_SRAM.h
 *
 *  Created on: 2016/3/1
 *      Author: Ivan_Lin
 */

#ifndef MALD37645A_SRAM_H_
#define MALD37645A_SRAM_H_

#define MALD37645A_SA          0x1E
#define MALD37645A_CHIPID      0x8F

extern void MALD37645_Init();
extern void MALD37645A_Write_All();
extern void MALD37645A_Read_ALL();

extern void Get_LUT_SET_BIAS_MOD();
extern void Get_Flash_CTLE_Table();

extern xdata unsigned char CTEL_Table[16];
extern void Bias_MOD_LUT_UpDate_Current();

extern unsigned int M37065_VCC33_ADC_MON();



xdata struct MALD37645A_MEMORY
{
	unsigned char CHIPID ;               // reg. address 0x00  R   SRAM Address 0X80
	unsigned char REVID  ;               // reg. address 0x01  R   SRAM Address 0X81
	unsigned char LOS_LOL_Fault_Status ; // reg. address 0x0B  R   SRAM Address 0X82
	unsigned char Bumin_Enable ;         // reg. address 0x28  R   SRAM Address 0X83
	unsigned char ADC_OUT0_MSB  ;        // reg. address 0x65  R   SRAM Address 0X84
	unsigned char ADC_OUT0_LSB ;         // reg. address 0x66  R   SRAM Address 0X85
	unsigned char RSVD[5];               // SRAM Address 0x86 - 0x8A

	unsigned char RESET ;                // reg. address 0x02  RW  SRAM Address 0x8B
	unsigned char MISC_IO_Control ;      // reg. address 0x03  RW  SRAM Address 0x8C
	unsigned char CDRCTRL  ;             // reg. address 0x04  RW  SRAM Address 0x8D
	unsigned char I2C_AD_MODE  ;         // reg. address 0x05  RW  SRAM Address 0x8E
	unsigned char CH_MODE ;              // reg. address 0x06  RW  SRAM Address 0x8F
	unsigned char LOCKPHASE ;            // reg. address 0x07  RW  SRAM Address 0x90
	unsigned char LOSLOL_Alarm ;         // reg. address 0x0C  RW  SRAM Address 0x91
	unsigned char TGNORE_FAULT ;         // reg. address 0x0D  RW  SRAM Address 0x92

	unsigned char LOS_THRSH_AUSQ ;       // reg. address 0x10  RW  SRAM Address 0x93
	unsigned char CTLE_X ;               // reg. address 0x12  RW  SRAM Address 0x94
	unsigned char OUTPUT_MUTE_SLEW ;     // reg. address 0x20  RW  SRAM Address 0x95
	unsigned char IBias ;                // reg. address 0x21  RW  SRAM Address 0x96
	unsigned char Imod ;                 // reg. address 0x22  RW  SRAM Address 0x97
	unsigned char PreFall ;              // reg. address 0x23  RW  SRAM Address 0x98

	unsigned char TDE ;                  // reg. address 0x24  RW  SRAM Address 0x99
	unsigned char Crossing_Adj ;         // reg. address 0x25  RW  SRAM Address 0x9A
	unsigned char IBumin ;               // reg. address 0x27  RW  SRAM Address 0x9B
	unsigned char ADC_Config0 ;          // reg. address 0x60  RW  SRAM Address 0x9C
	unsigned char ADC_Config2 ;          // reg. address 0x61  RW  SRAM Address 0x9D
	unsigned char ADC_Tx_Select ;        // reg. address 0x67  RW  SRAM Address 0x9E

	unsigned char Buffer[89] ;           // SRAM Address 0x9F - F7  < 89 Byte>

	unsigned char DC_AD;                 // SRAM Address 0xF8
	unsigned char DC_Data0;              // SRAM Address 0xF9
	unsigned char DC_Data1;              // SRAM Address 0xFA
	unsigned char DC_Data2;              // SRAM Address 0xFB
	unsigned char DC_Data3;              // SRAM Address 0xFC
	unsigned char DC_Data4;              // SRAM Address 0xFD
	unsigned char DC_RW;                 // SRAM Address 0xFE
	unsigned char DC_EN;                 // SRAM Address 0xFF

};

extern xdata struct MALD37645A_MEMORY MALD37645A_MEMORY_MAP;

#define MALD37645A_SIZE   sizeof(MALD37645A_MEMORY_MAP)

//extern uint8_t code MALD37045A_RX_Default_Table[];

#endif /* MALD37645A_SRAM_H_ */
