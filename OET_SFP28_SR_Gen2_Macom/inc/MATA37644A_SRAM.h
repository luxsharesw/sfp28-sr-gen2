/*
 * MA37044B_Map.h
 *
 *  Created on: 2016/2/29
 *      Author: Ivan_Lin
 */

#ifndef ATA37644A_MAP_H_
#define ATA37644A_MAP_H_
//#include <stdint.h>

#define MATA37644A_SA       0x1C
#define MATA37644A_CHIPID   0x8E

extern void MTTA37644_Init();
extern void MATA37644A_Write_All();
extern void MATA37644A_Read_ALL();
extern void Get_Flash_EMPHASIS_Table();

extern xdata unsigned char EMPHASIS_Table[16];

xdata struct MATA37644A_MEMORY
{
	unsigned char CHIPID ;               // reg. address 0x00  R   SRAM Address 0x80
	unsigned char REVID  ;               // reg. address 0x01  R   SRAM Address 0x81
	unsigned char LOS_LOL_STATUS ;       // reg. address 0x0B  R   SRAM Address 0x82
	unsigned char ADC_OUT0_MSB ;         // reg. address 0x65  R   SRAM Address 0x83
	unsigned char ADC_OUT0_LSB ;         // reg. address 0x66  R   SRAM Address 0x84

	unsigned char RSVD[4] ;              // SRAM Address 0x85 - 0x88

	unsigned char RESET ;                // reg. address 0x02  RW  SRAM Address 0x89
	unsigned char MONITORS  ;            // reg. address 0x03  RW  SRAM Address 0x8A
	unsigned char CDRCTRL  ;             // reg. address 0x04  RW  SRAM Address 0x8B
	unsigned char I2C_AD_MODE  ;         // reg. address 0x05  RW  SRAM Address 0x8C
	unsigned char CH_MODE  ;             // reg. address 0x06  RW  SRAM Address 0x8D
	unsigned char LOCK_PHASE  ;          // reg. address 0x07  RW  SRAM Address 0x8E
	unsigned char LOS_Mode  ;            // reg. address 0x0A  RW  SRAM Address 0x8F

	unsigned char LOS_LOL_ALARM  ;       // reg. address 0x0C  RW  SRAM Address 0x90
	unsigned char LOS_CTRL ;             // reg. address 0x10  RW  SRAM Address 0x91
	unsigned char SLA  ;                 // reg. address 0x11  RW  SRAM Address 0x92
	unsigned char TIA_CTRL ;             // reg. address 0x12  RW  SRAM Address 0x93
	unsigned char OUTPUT_CTRL ;          // reg. address 0x20  RW  SRAM Address 0x94
	unsigned char OUTPUT_SWING ;         // reg. address 0x21  RW  SRAM Address 0x95
	unsigned char OUTPUT_DEEMPH ;        // reg. address 0x22  RW  SRAM Address 0x96

	unsigned char ADC_Config0 ;          // reg. address 0x60  RW  SRAM Address 0x97
	unsigned char ADC_Config2 ;          // reg. address 0x61  RW  SRAM Address 0x98

	unsigned char Buffer[95] ;           // SRAM Address 0xA3 - 0xF7 95 Byte

	unsigned char DC_AD;                 // SRAM Address 0xF8
	unsigned char DC_Data0;              // SRAM Address 0xF9
	unsigned char DC_Data1;              // SRAM Address 0xFA
	unsigned char DC_Data2;              // SRAM Address 0xFB
	unsigned char DC_Data3;              // SRAM Address 0xFC
	unsigned char DC_Data4;              // SRAM Address 0xFD
	unsigned char DC_RW;                 // SRAM Address 0xFE
	unsigned char DC_EN;                 // SRAM Address 0xFF

};

extern xdata struct MATA37644A_MEMORY MATA37644A_MEMORY_MAP;
#define MATA37644A_SIZE   sizeof(MATA37644A_MEMORY_MAP)



#endif /* ATA37644A_MAP_H_ */
