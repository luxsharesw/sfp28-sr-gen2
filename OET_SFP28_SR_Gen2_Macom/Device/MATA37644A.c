/*
 * MATA37644A.c
 *
 *  Created on: 2017�~3��6��
 *      Author: 100476
 */

#include <MATA37644A_SRAM.h>
#include <Master_I2C_GPIO.h>
#include "Flash_MAP.h"
#include "MSA_SFF8472.h"
#include "EFM8LB_Flash_RW.h"
#include "Calibration_Struct.h"

xdata struct MATA37644A_MEMORY MATA37644A_MEMORY_MAP;

xdata unsigned char EMP_Index = 0 ;


void MATA37644A_RESET()
{
	MATA37644A_MEMORY_MAP.RESET = 0xAA ;
	Macom_WriteByte( MATA37644A_SA, 0x02 , MATA37644A_MEMORY_MAP.RESET ) ;
	Delay_1ms();
	MATA37644A_MEMORY_MAP.RESET = 0x00 ;
	Macom_WriteByte( MATA37644A_SA, 0x02 , MATA37644A_MEMORY_MAP.RESET ) ;
	Delay_1ms();
}

void MATA37644A_CHECKID()
{
	unsigned char DC_ID;
	unsigned char IIcount = 0;
	while(1)
	{
		DC_ID = Macom_ReadByte( MATA37644A_SA , 0x00 ) ;
		if( DC_ID == MATA37644A_SA )
			break;
		else
		{
			MATA37644A_RESET();
			IIcount++;
			if(IIcount>10)
			{
				CALIB_MEMORY_MAP.Error_Flag |= 0x02 ;
				break;
			}
		}
	}
}



void MATA37644A_Read_ALL()
{
	MATA37644A_MEMORY_MAP.CHIPID               = Macom_ReadByte( MATA37644A_SA , 0x00 ) ;
	MATA37644A_MEMORY_MAP.REVID                = Macom_ReadByte( MATA37644A_SA , 0x01 ) ;
	MATA37644A_MEMORY_MAP.LOS_LOL_STATUS       = Macom_ReadByte( MATA37644A_SA , 0x0B ) ;
	MATA37644A_MEMORY_MAP.ADC_OUT0_MSB         = Macom_ReadByte( MATA37644A_SA , 0x65 ) ;
	MATA37644A_MEMORY_MAP.ADC_OUT0_LSB         = Macom_ReadByte( MATA37644A_SA , 0x66 ) ;

	MATA37644A_MEMORY_MAP.MONITORS             = Macom_ReadByte( MATA37644A_SA , 0x03 ) ;
	MATA37644A_MEMORY_MAP.CDRCTRL              = Macom_ReadByte( MATA37644A_SA , 0x04 ) ;
	MATA37644A_MEMORY_MAP.I2C_AD_MODE          = Macom_ReadByte( MATA37644A_SA , 0x05 ) ;
	MATA37644A_MEMORY_MAP.CH_MODE              = Macom_ReadByte( MATA37644A_SA , 0x06 ) ;
	MATA37644A_MEMORY_MAP.LOCK_PHASE           = Macom_ReadByte( MATA37644A_SA , 0x07 ) ;
	MATA37644A_MEMORY_MAP.LOS_Mode             = Macom_ReadByte( MATA37644A_SA , 0x0A ) ;

	MATA37644A_MEMORY_MAP.LOS_LOL_ALARM        = Macom_ReadByte( MATA37644A_SA , 0x0C ) ;
	MATA37644A_MEMORY_MAP.LOS_CTRL             = Macom_ReadByte( MATA37644A_SA , 0x10 ) ;
	MATA37644A_MEMORY_MAP.SLA                  = Macom_ReadByte( MATA37644A_SA , 0x11 ) ;
	MATA37644A_MEMORY_MAP.TIA_CTRL             = Macom_ReadByte( MATA37644A_SA , 0x12 ) ;
	MATA37644A_MEMORY_MAP.OUTPUT_CTRL          = Macom_ReadByte( MATA37644A_SA , 0x20 ) ;
	MATA37644A_MEMORY_MAP.OUTPUT_SWING         = Macom_ReadByte( MATA37644A_SA , 0x21 ) ;
	MATA37644A_MEMORY_MAP.OUTPUT_DEEMPH        = Macom_ReadByte( MATA37644A_SA , 0x22 ) ;

	MATA37644A_MEMORY_MAP.ADC_Config0          = Macom_ReadByte( MATA37644A_SA , 0x60 ) ;
	MATA37644A_MEMORY_MAP.ADC_Config2          = Macom_ReadByte( MATA37644A_SA , 0x61 ) ;

}

void MATA37644A_Write_All()
{
	Macom_WriteByte( MATA37644A_SA, 0x03 , MATA37644A_MEMORY_MAP.MONITORS ) ;
	Macom_WriteByte( MATA37644A_SA, 0x04 , MATA37644A_MEMORY_MAP.CDRCTRL ) ;
	Macom_WriteByte( MATA37644A_SA, 0x05 , MATA37644A_MEMORY_MAP.I2C_AD_MODE ) ;
	Macom_WriteByte( MATA37644A_SA, 0x06 , MATA37644A_MEMORY_MAP.CH_MODE ) ;
	Macom_WriteByte( MATA37644A_SA, 0x07 , MATA37644A_MEMORY_MAP.LOCK_PHASE ) ;
	Macom_WriteByte( MATA37644A_SA, 0x0A , MATA37644A_MEMORY_MAP.LOS_Mode ) ;
	Macom_WriteByte( MATA37644A_SA, 0x0C , MATA37644A_MEMORY_MAP.LOS_LOL_ALARM ) ;

	Macom_WriteByte( MATA37644A_SA, 0x10 , MATA37644A_MEMORY_MAP.LOS_CTRL ) ;
	Macom_WriteByte( MATA37644A_SA, 0x11 , MATA37644A_MEMORY_MAP.SLA ) ;
	Macom_WriteByte( MATA37644A_SA, 0x12 , MATA37644A_MEMORY_MAP.TIA_CTRL ) ;
	Macom_WriteByte( MATA37644A_SA, 0x20 , MATA37644A_MEMORY_MAP.OUTPUT_CTRL ) ;
	Macom_WriteByte( MATA37644A_SA, 0x21 , MATA37644A_MEMORY_MAP.OUTPUT_SWING ) ;
	Macom_WriteByte( MATA37644A_SA, 0x22 , MATA37644A_MEMORY_MAP.OUTPUT_DEEMPH ) ;
	Macom_WriteByte( MATA37644A_SA, 0x60 , MATA37644A_MEMORY_MAP.ADC_Config0 ) ;
	Macom_WriteByte( MATA37644A_SA, 0x61 , MATA37644A_MEMORY_MAP.ADC_Config2 ) ;
}

void OUTPUT_EMPHASIS_Control(unsigned char EMP_Value , unsigned char HighLow_Rate_Select )
{
	unsigned char Control_Buffer;

	// A2 Byte115 Rate High 7 - 4  Low 3 - 0
	if( EMP_Index != EMP_Value )
	{
		if(HighLow_Rate_Select==1)
		{
			Control_Buffer = ( EMP_Value >> 4 );
			// Rate is High
			MATA37644A_MEMORY_MAP.OUTPUT_DEEMPH = EMPHASIS_Table[Control_Buffer] ;
			Macom_WriteByte( MATA37644A_SA, 0x22 , MATA37644A_MEMORY_MAP.OUTPUT_DEEMPH) ;

		}
		else
		{
			Control_Buffer = ( EMP_Value & 0x0F  );
			MATA37644A_MEMORY_MAP.OUTPUT_DEEMPH = EMPHASIS_Table[Control_Buffer] ;
			Macom_WriteByte( MATA37644A_SA, 0x22 , MATA37644A_MEMORY_MAP.OUTPUT_DEEMPH) ;
		}
	}
	EMP_Index = EMP_Value ;
}

void RX_CDR_Control( unsigned char  ONOFF_Control)
{
	unsigned char Contrl_Buffer ;

	Contrl_Buffer = MATA37644A_MEMORY_MAP.CH_MODE ;

	if( ONOFF_Control == 1 )
	{
		MATA37644A_MEMORY_MAP.CH_MODE &= 0xEF ;
		MATA37644A_MEMORY_MAP.TIA_CTRL &= 0xFB ;
	}
	else if ( ONOFF_Control == 0 )
	{
		MATA37644A_MEMORY_MAP.CH_MODE |= 0x10 ;
		MATA37644A_MEMORY_MAP.TIA_CTRL |= 0x04 ;
	}

	if( Contrl_Buffer != MATA37644A_MEMORY_MAP.CH_MODE )
	{
		Macom_WriteByte( MATA37644A_SA, 0x06 , MATA37644A_MEMORY_MAP.CH_MODE ) ;
		Macom_WriteByte( MATA37644A_SA, 0x12 , MATA37644A_MEMORY_MAP.TIA_CTRL ) ;
	}
}

void MTTA37644_Init()
{
	MATA37644A_RESET();
	MATA37644A_CHECKID();
	MATA37644A_Write_All();
//	MATA37644A_Read_ALL();
}


