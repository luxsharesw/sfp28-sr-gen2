/*
 * MALD37645A.c
 *
 *  Created on: 2017�~3��6��
 *      Author: 100476
 */

#include <MALD37645A_SRAM.h>
#include <Master_I2C_GPIO.h>
#include "Flash_MAP.h"
#include "MSA_SFF8472.h"
#include "EFM8LB_Flash_RW.h"
#include "Calibration_Struct.h"

xdata struct MALD37645A_MEMORY MALD37645A_MEMORY_MAP;
xdata unsigned char Bef_TXD_SW_FLAG = 0 ;
xdata unsigned char EQ_Index = 0 ;
xdata unsigned char Bef_Index_Temp = 0 ;


void MALD37645A_RESET()
{
	MALD37645A_MEMORY_MAP.RESET = 0xAA ;
	Macom_WriteByte( MALD37645A_SA, 0x02 , MALD37645A_MEMORY_MAP.RESET ) ;
	Delay_1ms();
	MALD37645A_MEMORY_MAP.RESET = 0x00 ;
	Macom_WriteByte( MALD37645A_SA, 0x02 , MALD37645A_MEMORY_MAP.RESET ) ;
	Delay_1ms();
}

void MALD37645A_CHECKID()
{
	unsigned char DC_ID;
	unsigned char IIcount = 0;
	while(1)
	{
		DC_ID = Macom_ReadByte( MALD37645A_SA , 0x00 ) ;
		if( DC_ID == MALD37645A_CHIPID )
			break;
		else
		{
			MALD37645A_RESET();
			IIcount++;
			if(IIcount>10)
			{
				CALIB_MEMORY_MAP.Error_Flag |= 0x01 ;
				break;
			}
		}
	}
}

void MALD37645A_Read_ALL()
{
	MALD37645A_MEMORY_MAP.CHIPID               = Macom_ReadByte( MALD37645A_SA , 0x00 ) ;
	MALD37645A_MEMORY_MAP.REVID                = Macom_ReadByte( MALD37645A_SA , 0x01 ) ;
	MALD37645A_MEMORY_MAP.LOS_LOL_Fault_Status = Macom_ReadByte( MALD37645A_SA , 0x0B ) ;
	MALD37645A_MEMORY_MAP.Bumin_Enable         = Macom_ReadByte( MALD37645A_SA , 0x28 ) ;
	MALD37645A_MEMORY_MAP.ADC_OUT0_MSB         = Macom_ReadByte( MALD37645A_SA , 0x65 ) ;
	MALD37645A_MEMORY_MAP.ADC_OUT0_LSB         = Macom_ReadByte( MALD37645A_SA , 0x66 ) ;

	MALD37645A_MEMORY_MAP.MISC_IO_Control      = Macom_ReadByte( MALD37645A_SA , 0x03 ) ;
	MALD37645A_MEMORY_MAP.CDRCTRL              = Macom_ReadByte( MALD37645A_SA , 0x04 ) ;
	MALD37645A_MEMORY_MAP.I2C_AD_MODE          = Macom_ReadByte( MALD37645A_SA , 0x05 ) ;
	MALD37645A_MEMORY_MAP.CH_MODE              = Macom_ReadByte( MALD37645A_SA , 0x06 ) ;
	MALD37645A_MEMORY_MAP.LOCKPHASE            = Macom_ReadByte( MALD37645A_SA , 0x07 ) ;
	MALD37645A_MEMORY_MAP.LOSLOL_Alarm         = Macom_ReadByte( MALD37645A_SA , 0x0C ) ;
	MALD37645A_MEMORY_MAP.TGNORE_FAULT         = Macom_ReadByte( MALD37645A_SA , 0x0D ) ;

	MALD37645A_MEMORY_MAP.LOS_THRSH_AUSQ       = Macom_ReadByte( MALD37645A_SA , 0x10 ) ;
	MALD37645A_MEMORY_MAP.CTLE_X               = Macom_ReadByte( MALD37645A_SA , 0x12 ) ;
	MALD37645A_MEMORY_MAP.OUTPUT_MUTE_SLEW     = Macom_ReadByte( MALD37645A_SA , 0x20 ) ;
	MALD37645A_MEMORY_MAP.IBias                = Macom_ReadByte( MALD37645A_SA , 0x21 ) ;
	MALD37645A_MEMORY_MAP.Imod                 = Macom_ReadByte( MALD37645A_SA , 0x22 ) ;
	MALD37645A_MEMORY_MAP.PreFall              = Macom_ReadByte( MALD37645A_SA , 0x23 ) ;

	MALD37645A_MEMORY_MAP.TDE                  = Macom_ReadByte( MALD37645A_SA , 0x24 ) ;
	MALD37645A_MEMORY_MAP.Crossing_Adj         = Macom_ReadByte( MALD37645A_SA , 0x25 ) ;
	MALD37645A_MEMORY_MAP.IBumin               = Macom_ReadByte( MALD37645A_SA , 0x27 ) ;
	MALD37645A_MEMORY_MAP.ADC_Config0          = Macom_ReadByte( MALD37645A_SA , 0x60 ) ;
	MALD37645A_MEMORY_MAP.ADC_Config2          = Macom_ReadByte( MALD37645A_SA , 0x61 ) ;
	MALD37645A_MEMORY_MAP.ADC_Tx_Select        = Macom_ReadByte( MALD37645A_SA , 0x67 ) ;

}

void MALD37645A_Write_All()
{
	Macom_WriteByte( MALD37645A_SA, 0x02 , MALD37645A_MEMORY_MAP.RESET ) ;
	Macom_WriteByte( MALD37645A_SA, 0x03 , MALD37645A_MEMORY_MAP.MISC_IO_Control ) ;
	Macom_WriteByte( MALD37645A_SA, 0x04 , MALD37645A_MEMORY_MAP.CDRCTRL ) ;
	Macom_WriteByte( MALD37645A_SA, 0x05 , MALD37645A_MEMORY_MAP.I2C_AD_MODE ) ;
	Macom_WriteByte( MALD37645A_SA, 0x06 , MALD37645A_MEMORY_MAP.CH_MODE ) ;
	Macom_WriteByte( MALD37645A_SA, 0x07 , MALD37645A_MEMORY_MAP.LOCKPHASE ) ;
	Macom_WriteByte( MALD37645A_SA, 0x0C , MALD37645A_MEMORY_MAP.LOSLOL_Alarm ) ;
	Macom_WriteByte( MALD37645A_SA, 0x0D , MALD37645A_MEMORY_MAP.TGNORE_FAULT ) ;

	Macom_WriteByte( MALD37645A_SA, 0x10 , MALD37645A_MEMORY_MAP.LOS_THRSH_AUSQ ) ;
	Macom_WriteByte( MALD37645A_SA, 0x12 , MALD37645A_MEMORY_MAP.CTLE_X ) ;
	Macom_WriteByte( MALD37645A_SA, 0x20 , MALD37645A_MEMORY_MAP.OUTPUT_MUTE_SLEW ) ;
	Macom_WriteByte( MALD37645A_SA, 0x21 , MALD37645A_MEMORY_MAP.IBias ) ;


	Macom_WriteByte( MALD37645A_SA, 0x23 , MALD37645A_MEMORY_MAP.PreFall ) ;
	Macom_WriteByte( MALD37645A_SA, 0x24 , MALD37645A_MEMORY_MAP.TDE ) ;
	Macom_WriteByte( MALD37645A_SA, 0x25 , MALD37645A_MEMORY_MAP.Crossing_Adj ) ;
	Macom_WriteByte( MALD37645A_SA, 0x27 , MALD37645A_MEMORY_MAP.IBumin ) ;
	Macom_WriteByte( MALD37645A_SA, 0x28 , MALD37645A_MEMORY_MAP.Bumin_Enable ) ;

	Macom_WriteByte( MALD37645A_SA, 0x60 , MALD37645A_MEMORY_MAP.ADC_Config0 ) ;
	Macom_WriteByte( MALD37645A_SA, 0x61 , MALD37645A_MEMORY_MAP.ADC_Config2 ) ;
	Macom_WriteByte( MALD37645A_SA, 0x67 , MALD37645A_MEMORY_MAP.ADC_Tx_Select ) ;

	Macom_WriteByte( MALD37645A_SA, 0x22 , MALD37645A_MEMORY_MAP.Imod ) ;
}

void Get_LUT_SET_BIAS_MOD()
{
	if( ( Temperature_Indx >=0 ) && ( Temperature_Indx < 64 ) )
	{
		MALD37645A_MEMORY_MAP.IBias = FLASH_ByteRead( Bias_LUT_0 + Temperature_Indx ) ;
		MALD37645A_MEMORY_MAP.Imod = FLASH_ByteRead( Mod_LUT_0 + Temperature_Indx ) ;

		Macom_WriteByte( MALD37645A_SA, 0x21 , MALD37645A_MEMORY_MAP.IBias ) ;
		Macom_WriteByte( MALD37645A_SA, 0x22 , MALD37645A_MEMORY_MAP.Imod ) ;

		MALD37645A_MEMORY_MAP.IBias = Macom_ReadByte( MALD37645A_SA , 0x21 ) ;
		MALD37645A_MEMORY_MAP.Imod = Macom_ReadByte( MALD37645A_SA , 0x22 ) ;
	}
}

unsigned int Get_Ibias_Current()
{
	// Set Reg0x60h=00h, Reg0x67h=02h (ADC connected to IBIAS monitor reference node)
	// Read Reg0x65h[7:0] and Reg0x66h[3:0], store this as VAR2
	// Reg0x67h=03h (ADC connected to IBIAS monitor measurement node)
	// Read Reg0x65h[7:0] and Reg0x66h[3:0], store this as VAR3
	// Calculate IBIAS = (VAR3 - VAR2) x VAR1 x 40.1uA for Datasheet
	// Calculate IBIAS = (VAR2 - VAR3) x VAR1 x 40.1uA for Measured
	// For example V1 read as 0.92V, VAR1 = 0.92/0.832 = 1.11, VAR2 = 993d, VAR3 = 1150d
	// IBIAS = (1150 - 993) x 1.11 x 42.5 = 7.41mA

	unsigned int Bias_Current ;
	unsigned int M37645_VR2,M37645_VR3;
	unsigned int Imod = 0 ;
	unsigned char IIcount = 0 ;
//	unsigned int VCC_33V ;

	MALD37645A_MEMORY_MAP.ADC_Config0 = 0x00 ;
	MALD37645A_MEMORY_MAP.ADC_Tx_Select = 0x02 ;
	//Macom_WriteByte( MALD37645A_SA, 0x60 , MALD37645A_MEMORY_MAP.ADC_Config0 ) ;

	Macom_WriteByte( MALD37645A_SA, 0x67 , MALD37645A_MEMORY_MAP.ADC_Tx_Select ) ;
	// important : 0x67 write after read 0x68 0x69 two byte
	M37645_VR2 = Macom_Read_Two_Byte( MALD37645A_SA , 0x68 );
	M37645_VR2 = Macom_Read_Two_Byte( MALD37645A_SA , 0x65 );

	MALD37645A_MEMORY_MAP.ADC_Tx_Select = 0x03 ;
	Macom_WriteByte( MALD37645A_SA, 0x67 , MALD37645A_MEMORY_MAP.ADC_Tx_Select ) ;
	// important : 0x67 write after read 0x68 0x69 two byte
	M37645_VR3 = Macom_Read_Two_Byte( MALD37645A_SA , 0x68 );
	M37645_VR3 = Macom_Read_Two_Byte( MALD37645A_SA , 0x65 );


	Bias_Current = ( M37645_VR2 - M37645_VR3 )*(42.1);
	// Get Imod Current
//	Imod = MALD37645A_MEMORY_MAP.Imod ;
//	Imod = ( 67*Imod )/2;
//	Bias_Current = Bias_Current + Imod;
	// MSA 2uA unit
	Bias_Current = Bias_Current / 2 ;

	return Bias_Current;
}

unsigned int Get_TXPOWER_Value()
{
	unsigned TXP_Value;

	return TXP_Value;
}

void TXD_Function()
{
	if( TXD_SW_FLAG != Bef_TXD_SW_FLAG )
	{
		if(TXD_SW_FLAG==1)
		{
			MALD37645A_MEMORY_MAP.OUTPUT_MUTE_SLEW |= 0x02;
			Macom_WriteByte( MALD37645A_SA, 0x20 , MALD37645A_MEMORY_MAP.OUTPUT_MUTE_SLEW ) ;
		}
		else
		{
			MALD37645A_MEMORY_MAP.OUTPUT_MUTE_SLEW &= ~0x02;
			Macom_WriteByte( MALD37645A_SA, 0x20 , MALD37645A_MEMORY_MAP.OUTPUT_MUTE_SLEW ) ;
		}
	}

	Bef_TXD_SW_FLAG = TXD_SW_FLAG;
}

void CTLE_Control(unsigned char MSA_Vaule,unsigned char HighLow_Rate_Select)
{
	unsigned char Control_Buffer;

	if( EQ_Index != MSA_Vaule )
	{
		if(HighLow_Rate_Select==1)
		{
			Control_Buffer = ( MSA_Vaule >> 4 ) ;
			MALD37645A_MEMORY_MAP.CTLE_X = CTEL_Table[ Control_Buffer ];
			Macom_WriteByte( MALD37645A_SA, 0x12 , MALD37645A_MEMORY_MAP.CTLE_X ) ;

		}
		else
		{
			Control_Buffer = ( MSA_Vaule & 0x0F ) ;
			MALD37645A_MEMORY_MAP.CTLE_X = CTEL_Table[ Control_Buffer ];
			Macom_WriteByte( MALD37645A_SA, 0x12 , MALD37645A_MEMORY_MAP.CTLE_X ) ;
		}
	}
	EQ_Index = MSA_Vaule ;
}

void Tx_CDR_Control( unsigned char  ONOFF_Control)
{
	unsigned char Contrl_Buffer ;
	Contrl_Buffer = MALD37645A_MEMORY_MAP.CH_MODE ;

	// TX CDR Turn on
	if( ONOFF_Control == 1 )
		MALD37645A_MEMORY_MAP.CH_MODE &= 0xEF;
	// TX CDR Bypass and Power down
	else if( ONOFF_Control == 0 )
		MALD37645A_MEMORY_MAP.CH_MODE |= 0x10;

	if(Contrl_Buffer!=MALD37645A_MEMORY_MAP.CH_MODE)
		Macom_WriteByte( MALD37645A_SA, 0x06 , MALD37645A_MEMORY_MAP.CH_MODE ) ;

}


void Bias_MOD_LUT_UpDate_Current()
{
	if( Bef_Index_Temp != Temperature_Indx )
	{
		MALD37645A_MEMORY_MAP.IBias = FLASH_ByteRead( Bias_LUT_0 + Temperature_Indx ) ;
		MALD37645A_MEMORY_MAP.Imod = FLASH_ByteRead( Mod_LUT_0 + Temperature_Indx ) ;

		Macom_WriteByte( MALD37645A_SA, 0x21 , MALD37645A_MEMORY_MAP.IBias ) ;
		Macom_WriteByte( MALD37645A_SA, 0x22 , MALD37645A_MEMORY_MAP.Imod ) ;

		MALD37645A_MEMORY_MAP.IBias = Macom_ReadByte( MALD37645A_SA , 0x21 ) ;
		MALD37645A_MEMORY_MAP.Imod = Macom_ReadByte( MALD37645A_SA , 0x22 ) ;
	}

	Bef_Index_Temp = Temperature_Indx ;
}

unsigned int M37065_VCC33_ADC_MON()
{
	unsigned int VCC33_ADC_Value = 0 ;
	// VCC33 Monitor
	// Set Reg0x60h[2:0] = 000b and Reg0x67h[1:0] = 00b (ADC monitors VCC33)
	// Confirm that Reg0x61h[0] = 0b (ADC Operating normally)
	// Read Reg0x65h[7:0] & Reg0x66h[3:0]. (ADC output code bits [11:4] and [3:0])
	// ADC Code = The decimal value of Reg0x65h[7:0] and Reg0x66h[3:0] added together
	// VCC33 = ADC_VALUE / 594

	// Step1 : 0x60 write to 0x00
	Macom_WriteByte( MALD37645A_SA , 0x60 , 0x00 );
	// Step2 : 0x67 write to 0x00
	Macom_WriteByte( MALD37645A_SA , 0x67 , 0x00 );
	// Step3 : Confirm 0x61 bit0 0
	if( (Macom_ReadByte(MALD37645A_SA,0x61) & 0x01 ) == 0x00 )
	{
		VCC33_ADC_Value = Macom_ReadByte(MALD37645A_SA,0x65);
		VCC33_ADC_Value = VCC33_ADC_Value + (unsigned int)( Macom_ReadByte(MALD37645A_SA,0x66)<<8 ) ;

		VCC33_ADC_Value = VCC33_ADC_Value/594 ;
	}
	return VCC33_ADC_Value;
}

void MALD37645_Init()
{
	MALD37645A_RESET();
	MALD37645A_CHECKID();
	MALD37645A_Write_All();
//	MALD37645A_Read_ALL();
}



