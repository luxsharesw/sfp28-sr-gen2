/*
 * Flash_RW.c
 *
 *  Created on: 2017�~3��8��
 *      Author: 100476
 */
/*
//-----------------------------------------------------------------------------
// Includes
//-----------------------------------------------------------------------------
#include <SI_EFM8LB1_Register_Enums.h>
#include "EFM8LB1_FlashPrimitives.h"

//-----------------------------------------------------------------------------
// FLASH_ByteWrite
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   :
//   1) FLADDR addr - address of the byte to write to
//                    valid range is from 0x0000 to 0x1FFF for 8 kB devices
//                    valid range is from 0x0000 to 0x0FFF for 4 kB devices
//                    valid range is from 0x0000 to 0x07FF for 2 kB devices
//   2) uint8_t - byte to write to flash.
//
// This routine writes <byte> to the linear flash address <addr>.
//
//-----------------------------------------------------------------------------
void FLASH_ByteWrite (FLADDR addr, uint8_t byte)
{
	bool EA_SAVE = IE_EA;                // Preserve IE_EA
	SI_VARIABLE_SEGMENT_POINTER(pwrite, uint8_t, SI_SEG_XDATA); // Flash write pointer

	unsigned char SFRPAGE_SAVE = SFRPAGE ;
	SFRPAGE = 0x00 ;

	IE_EA = 0;                          // Disable interrupts

	VDM0CN = 0x80;                      // Enable VDD monitor

	RSTSRC = 0x02;                      // Enable VDD monitor as a reset source

	pwrite = (SI_VARIABLE_SEGMENT_POINTER(, uint8_t, SI_SEG_XDATA)) addr;

	FLKEY  = 0xA5;                      // Key Sequence 1
	FLKEY  = 0xF1;                      // Key Sequence 2
	PSCTL |= 0x01;                      // PSWE = 1 which enables writes

	VDM0CN = 0x80;                      // Enable VDD monitor

	RSTSRC = 0x02;                      // Enable VDD monitor as a reset source
	*pwrite = byte;                     // Write the byte

	PSCTL &= ~0x01;                     // PSWE = 0 which disable writes

	IE_EA = EA_SAVE;                    // Restore interrupts
	SFRPAGE = SFRPAGE_SAVE ;
}

//-----------------------------------------------------------------------------
// FLASH_ByteRead
//-----------------------------------------------------------------------------
//
// Return Value :
//      uint8_t - byte read from flash
// Parameters   :
//   1) FLADDR addr - address of the byte to read to
//                    valid range is from 0x0000 to 0x1FFF for 8 kB devices
//                    valid range is from 0x0000 to 0x0FFF for 4 kB devices
//                    valid range is from 0x0000 to 0x07FF for 2 kB devices
//
// This routine reads a <byte> from the linear flash address <addr>.
//
//-----------------------------------------------------------------------------
uint8_t FLASH_ByteRead (FLADDR addr)
{
	bool EA_SAVE = IE_EA;                // Preserve IE_EA
	SI_VARIABLE_SEGMENT_POINTER(pread, uint8_t, const SI_SEG_CODE); // Flash read pointer

	uint8_t byte;
	uint8_t SFRPAGE_SAVE = SFRPAGE ;
	SFRPAGE = 0x00 ;

	IE_EA = 0;                          // Disable interrupts

	pread = (SI_VARIABLE_SEGMENT_POINTER(, uint8_t, const SI_SEG_CODE)) addr;

	byte = *pread;                      // Read the byte

	IE_EA = EA_SAVE;                    // Restore interrupts
	SFRPAGE = SFRPAGE_SAVE ;
	return byte;
}

//-----------------------------------------------------------------------------
// FLASH_PageErase
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   :
//   1) FLADDR addr - address of any byte in the page to erase
//                    valid range is from 0x0000 to 0x1FFF for 8 kB devices
//                    valid range is from 0x0000 to 0x0FFF for 4 kB devices
//                    valid range is from 0x0000 to 0x07FF for 2 kB devices
//
// This routine erases the flash page containing the linear flash address
// <addr>.  Note that the page of flash page containing the Lock Byte cannot be
// erased if the Lock Byte is set.
//
//-----------------------------------------------------------------------------
void FLASH_PageErase (FLADDR addr)
{
	bool EA_SAVE = IE_EA;                // Preserve IE_EA
	SI_VARIABLE_SEGMENT_POINTER(pwrite, uint8_t, SI_SEG_XDATA); // Flash write pointer

	unsigned char SFRPAGE_SAVE = SFRPAGE ;
	SFRPAGE = 0x00 ;

	IE_EA = 0;                          // Disable interrupts

	VDM0CN = 0x80;                      // Enable VDD monitor

	RSTSRC = 0x02;                      // Enable VDD monitor as a reset source

	pwrite = (SI_VARIABLE_SEGMENT_POINTER(, uint8_t, SI_SEG_XDATA)) addr;

	FLKEY  = 0xA5;                      // Key Sequence 1
	FLKEY  = 0xF1;                      // Key Sequence 2
	PSCTL |= 0x03;                      // PSWE = 1; PSEE = 1

	VDM0CN = 0x80;                      // Enable VDD monitor

	RSTSRC = 0x02;                      // Enable VDD monitor as a reset source
	*pwrite = 0;                        // Initiate page erase

	PSCTL &= ~0x03;                     // PSWE = 0; PSEE = 0

	IE_EA = EA_SAVE;                    // Restore interrupts
	SFRPAGE = SFRPAGE_SAVE ;
}

void Flash_EW_NoErase(FLADDR dest, uint8_t *src, uint16_t numbytes)
{
	FLADDR i;
	for (i = dest; i < dest+numbytes; i++)
		FLASH_ByteWrite (i, *src++);

}

void Flash_EWFunction(FLADDR dest, uint8_t *src, uint16_t numbytes)
{
	FLADDR i;
	FLASH_PageErase(dest);

	for (i = dest; i < dest+numbytes; i++)
		FLASH_ByteWrite (i, *src++);
}

void Flash_ReadFunction(FLADDR src,uint8_t *dest,  uint16_t numbytes)
{
	FLADDR i;
	for (i = 0; i < numbytes; i++)
		*dest++ = FLASH_ByteRead (src+i);
}

*/
#include <SI_EFM8LB1_Register_Enums.h>

//-----------------------------------------------------------------------------
// FLASH_ByteWrite
//-----------------------------------------------------------------------------
void FLASH_ByteWrite(unsigned int addr, char byte,char Check_byte)
{
	unsigned char SFRPAGE_SAVE = SFRPAGE ;   // preserve SFRPAGE
	bit EA_SAVE = IE_EA;                     // preserve EA
	char xdata * data pwrite;                // FLASH write pointer

	SFRPAGE = 0x00 ;                         // modify SFRPAGE = 0x00

	IE_EA = 0;                               // disable interrupts
                                             // change clock speed to slow, then restore later
    VDM0CN = 0x80;                           // enable VDD monitor
    RSTSRC = 0x02;                           // enable VDD monitor as a reset source

    pwrite = (char xdata *) addr;
	if(Check_byte==0x55)
   	{
		FLKEY  = 0xA5;                       // Key Sequence 1
    	FLKEY  = 0xF1;                       // Key Sequence 2
   		PSCTL |= 0x01;                       // PSWE = 1
	}
    VDM0CN = 0x80;                           // enable VDD monitor
    RSTSRC = 0x02;                           // enable VDD monitor as a reset source

    *pwrite = byte;                          // write the byte

    PSCTL &= ~0x01;                          // PSWE = 0
	Check_byte = 0 ;

	IE_EA = EA_SAVE;                         // restore interrupts

	SFRPAGE = SFRPAGE_SAVE ;                 // restore SFRPAGE_SAVE
}

//-----------------------------------------------------------------------------
// FLASH_ByteRead
//-----------------------------------------------------------------------------
unsigned char FLASH_ByteRead(unsigned int addr)
{
	unsigned char SFRPAGE_SAVE = SFRPAGE ;
    bit EA_SAVE = IE_EA;                          // preserve EA

    char code * data pread;                       // FLASH read pointer
    unsigned char byte;

    IE_EA = 0;                                    // disable interrupts

    SFRPAGE = 0x00 ;                              // modify SFRPAGE = 0x00

    pread = (char code *) addr;
    byte = *pread;                                // read the byte

    IE_EA = EA_SAVE;                              // restore interrupts

    SFRPAGE = SFRPAGE_SAVE ;                      // restore SFRPAGE_SAVE

    return byte;
}

//-----------------------------------------------------------------------------
// FLASH_PageErase
//-----------------------------------------------------------------------------
void FLASH_PageErase(unsigned int addr,char Check_byte)
{
	unsigned char SFRPAGE_SAVE = SFRPAGE ;   // preserve SFRPAGE
    bit EA_SAVE = IE_EA;                     // preserve EA
    char xdata * data pwrite;                // FLASH write pointer

    IE_EA = 0;                               // Disable interrupts
    										 // Change clock speed to slow, then restore later
    SFRPAGE = 0x00 ;                         // modify SFRPAGE = 0x00

    VDM0CN = 0x80;                           // Enable VDD monitor
    RSTSRC = 0x02;                           // Enable VDD monitor as a reset source
    pwrite = (char xdata *) addr;
    if(Check_byte==0x55)
    {
		FLKEY  = 0xA5;                       // Key Sequence 1
    	FLKEY  = 0xF1;                       // Key Sequence 2
    	PSCTL |= 0x03;                       // PSWE = 1; PSEE = 1
	}
    VDM0CN = 0x80;                           // enable VDD monitor
    RSTSRC = 0x02;                           // enable VDD monitor as a reset source

    *pwrite = 0;                             // initiate page erase
    PSCTL &= ~0x03;                          // PSWE = 0; PSEE = 0
	Check_byte = 0 ;

	IE_EA = EA_SAVE;                         // restore interrupts
	SFRPAGE = SFRPAGE_SAVE ;                 // restore SFRPAGE_SAVE
}

void Flash_EW_NoErase(unsigned int FlashAddress,unsigned char *Data_Buffer, unsigned int DataL)
{
	unsigned int Data_Count_L = 0;

	for( Data_Count_L=0 ; Data_Count_L<DataL ; Data_Count_L++ )
	{
		FLASH_ByteWrite( ( FlashAddress + Data_Count_L ) , *Data_Buffer++ , 0x55 );
	}

}

void Flash_EWFunction(unsigned int FlashAddress,unsigned char *Data_Buffer, unsigned int DataL)
{
	unsigned int Data_Count_L = 0;
	FLASH_PageErase( FlashAddress , 0x55 );

	for( Data_Count_L=0 ; Data_Count_L<DataL ; Data_Count_L++ )
	{
		FLASH_ByteWrite( ( FlashAddress + Data_Count_L ) , *Data_Buffer++ , 0x55 );
	}

}

void Flash_ReadFunction(unsigned int FlashAddress,unsigned char *Data_Buffer, unsigned int DataL)
{
	unsigned int Data_Count_L = 0;
	for( Data_Count_L=0 ; Data_Count_L<DataL ; Data_Count_L++ )
	{
		*Data_Buffer++ = FLASH_ByteRead(FlashAddress+Data_Count_L) ;
	}
}







