//-----------------------------------------------------------------------------
// Includes
//-----------------------------------------------------------------------------
#include <SI_EFM8LB1_Register_Enums.h>                  // SFR declarations
#include "InitDevice.h"
#include <MALD37645A_SRAM.h>
#include <MATA37644A_SRAM.h>
#include "MSA_SFF8472.h"
#include "Calibration_Struct.h"
// $[Generated Includes]
// [Generated Includes]$
#define EN_LUT   0

extern void Tempsonr_0C_internal_Cal();
extern void PowerOn_Table_Init();
extern void Delay_ms(int time_ms);
extern void SRMA_Flash_Function();
extern void Power_Sequencing();
//-----------------------------------------------------------------------------
// SiLabs_Startup() Routine
// ----------------------------------------------------------------------------
// This function is called immediately after reset, before the initialization
// code is run in SILABS_STARTUP.A51 (which runs before main() ). This is a
// useful place to disable the watchdog timer, which is enable by default
// and may trigger before main() in some instances.
//-----------------------------------------------------------------------------
void SiLabs_Startup(void)
{
	WDT_0_enter_DefaultMode_from_RESET();
}
//-----------------------------------------------------------------------------
// main() Routine
// ----------------------------------------------------------------------------
int main(void)
{
	unsigned int MIIcount = 0;
	// Call hardware initialization routine
	enter_DefaultMode_from_RESET();

	Power_Sequencing();

	Delay_ms(60);

	Tempsonr_0C_internal_Cal();
	PowerOn_Table_Init();

	MALD37645_Init();
	MTTA37644_Init();

	CTLE_Control(SFF8472_A2[114], 1);
	OUTPUT_EMPHASIS_Control(SFF8472_A2[115], 1);

	// Rate_Select_Control();
	// Device initialization
	IE_EA = 1;		// Enable All Interrupt.

	while (1)
	{
		//  1 ms update DDMI Data
		if( MIIcount > 1200 )
		{
			SFF8472_Monitor();
			SFF8472_AW_Function();
			TXD_Function();

			MIIcount = 0;
		}
		MIIcount++;
		CTLE_Control(SFF8472_A2[114], 1);
		OUTPUT_EMPHASIS_Control(SFF8472_A2[115], 1);
		// SRAM & Flash Function
		SRMA_Flash_Function();
	}
}


