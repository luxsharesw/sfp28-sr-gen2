/*
 * Power_Sequencing.c
 *
 *  Created on: 2019�~7��16��
 *      Author: Ivan_Lin
 */
#include <SI_EFM8LB1_Register_Enums.h>                  // SFR declarations
#include "InitDevice.h"
#include "MSA_SFF8472.h"
#include "Calibration_Struct.h"

SI_SBIT( CDR_EN_P , SFR_P1, 0 );
SI_SBIT( TXDIS    , SFR_P0, 6 );
SI_SBIT( CDR_RSC  , SFR_P1, 3 );

SI_SBIT( RS0_MCU , SFR_P0, 5 );
SI_SBIT( RS1_MCU , SFR_P0, 4 );

extern void Delay_ms(int time_ms);

xdata unsigned char RataSelect_flg = 3;

void CDR_POWER_ON()
{
	CDR_EN_P = 0;
	Delay_ms(5);

	CDR_EN_P = 1;
	Delay_ms(5);
	// RS = 1 -> 25G
	// RS = 0 -> 8.5G or 14.025G
	CDR_RSC = 1;
	// TRX RESET
//	TRX_RESET = 0;
//	Delay_ms(2);
//	TRX_RESET = 1;
	TXDIS = 0;
}

void GPIO_Rate_Select_Status()
{
	bit Soft_RS0, Soft_RS1;

	// A2 110 RS0
	if (SFF8472_A2[110] & 0x08)
		Soft_RS0 = 1;
	else
		Soft_RS0 = 0;

	// A2 118 RS1
	if (SFF8472_A2[118] & 0x08)
		Soft_RS1 = 1;
	else
		Soft_RS1 = 0;

	if ((RS0_MCU & RS1_MCU) || (Soft_RS0 & Soft_RS1))
		RataSelect_flg = 3;                          // TRX CDR Turn on
	else if (((RS0_MCU == 1) & (RS1_MCU == 0))|| ((Soft_RS0 == 1) & (Soft_RS1 == 0)))
		RataSelect_flg = 2;                          // TRX CDR Bypass
	else if (((RS0_MCU == 0) & (RS1_MCU == 1))|| ((Soft_RS0 == 0) & (Soft_RS1 == 1)))
		RataSelect_flg = 1;                      // RX CDR Turn on Tx CDR Bypass
	else if (((RS0_MCU == 0) & (RS1_MCU == 0))|| ((Soft_RS0 == 0) & (Soft_RS1 == 0)))
		RataSelect_flg = 2;            	          // TRX CDR Bypass

	CALIB_MEMORY_MAP.DEBUG_TEMP1 = RataSelect_flg;
}

void Rate_Select_Control()
{
	GPIO_Rate_Select_Status();
	if (RataSelect_flg == 3)
	{
		// TRX CDR Turn on
		Tx_CDR_Control(1);
		RX_CDR_Control(1);
		CTLE_Control(SFF8472_A2[114], 1);
		OUTPUT_EMPHASIS_Control(SFF8472_A2[115], 1);
		CDR_LOL_Status(1);
	}
	else if (RataSelect_flg == 2)
	{
		// TRX CDR Bypass
		Tx_CDR_Control(0);
		RX_CDR_Control(0);
		CTLE_Control(SFF8472_A2[114], 0);
		OUTPUT_EMPHASIS_Control(SFF8472_A2[115], 0);
		SFF8472_A2[119] = 0;
	}
	else if (RataSelect_flg == 1)
	{
		// TX CDR Bypass
		Tx_CDR_Control(0);
		// RX CDR Turn on
		RX_CDR_Control(1);

		CTLE_Control(SFF8472_A2[114], 0);
		OUTPUT_EMPHASIS_Control(SFF8472_A2[115], 1);
		CDR_LOL_Status(2);
		SFF8472_A2[119] &= 0xFD;
	}
}

void Power_Sequencing()
{
	CDR_POWER_ON();
}


