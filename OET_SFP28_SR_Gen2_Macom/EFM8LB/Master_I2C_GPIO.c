#include <Master_I2C_GPIO.h>
#include <SI_EFM8LB1_Register_Enums.h>                  // SFR declarations

#define Low  0
#define High 1

#define Slave_ACK  0
#define Slave_nACK 1

SI_SBIT( Master_SDA , SFR_P2, 2);
SI_SBIT( Master_SCL , SFR_P2, 3);


void Delay_1us(void)
{
	int i ;
    for(i=0;i<10;i++);
}

void Delay_us(unsigned char time_us)
{
	int i;
	for(i=0;i<time_us;i++)
		Delay_1us();
}

void Delay_1ms(void)
{
	int i ;
    for(i=0; i < 4500; i++)
    	_nop_();
}
void Delay_ms(int time_ms)
{
	int i;
	for(i = 0; i < time_ms; i++)
		Delay_1ms();
}

void I2C_Idle(void)
{
	Master_SCL = High;
	Master_SDA = High;
	Delay_us(1);
}

void I2C_Start(void)
{
	Delay_1us();
	Master_SDA = Low;
	Delay_us(2);
}

void I2C_Stop(void)
{
	Master_SDA = Low;
	Delay_us(2);

	Master_SCL = High;
	Delay_us(2);

	Master_SDA = High;
	Delay_1us();
}

unsigned char I2C_ReadByte(void)
{
	bit buffer;
	char i;
	unsigned char Rec_Data=0;

	Master_SCL = Low;
	Delay_1us();
	Delay_1us();
	Delay_1us();
	Delay_1us();

	for(i=7;i>=0;i--)
	{
		Delay_1us();
		Master_SCL = High;
		Delay_1us();
		Delay_1us();
		buffer = Master_SDA;
		Rec_Data |= ((unsigned char)(buffer) << i);
	//	Delay_1us();
		Master_SCL = Low;
		Delay_1us();
	}
	return Rec_Data;
}

void I2C_WriteByte(unsigned char Reg_Data)
{
	char i;
	unsigned char Temp_Data=Reg_Data;

	Master_SCL = Low;
	Delay_1us();
	Delay_1us();
	Delay_1us();
	Delay_1us();

	for(i=7;i>=0;i--)
	{
		Temp_Data = ((Reg_Data >> i) & 0x01);
		if(Temp_Data)
			Master_SDA = High;
		else
			Master_SDA = Low;
	//	Delay_1us();
		Master_SCL = High;
		Delay_1us();
		Delay_1us();
		Master_SCL = Low;
		Delay_1us();
	}
}

void I2C_nACK_Master(void)
{
	Delay_1us();
	Master_SDA = High;
	Delay_1us();
	Master_SCL = High;
	Delay_1us();
	Delay_1us();
	Master_SCL = Low;
	Delay_1us();
	Master_SDA = Low;
	Delay_1us();
}

void I2C_ACK_Slave(void)
{
	Delay_1us();
	Master_SCL = High;
	Delay_1us();
	Delay_1us();

	if(Master_SDA)
	{
		Master_SCL = Low;
		Delay_1us();
		Master_SDA = Low;
		Delay_1us();

	}
	else
	{
		Master_SCL = Low;
		Delay_1us();
		Master_SDA = High;
		Delay_1us();

	}
}

void I2C_MRSlave_Ack()
{
	Master_SDA = Low ;
	Master_SCL = High ;
	Delay_1us();
	Delay_1us();
	Master_SCL = Low ;
	Master_SDA = High ;
}


void I2C_MRSlave_NAck()
{
	Master_SDA = High ;
	Master_SCL = High ;
	Delay_1us();
	Delay_1us();
	Master_SCL = Low ;
}

unsigned char Macom_ReadByte(unsigned char Slave_address,unsigned char Regcommand_Address)
{
	unsigned char Temp_Data;
	I2C_Idle();
	I2C_Start();
	I2C_WriteByte(Slave_address);		// Write
	I2C_ACK_Slave();

	I2C_WriteByte(Regcommand_Address);
	I2C_ACK_Slave();


	Master_SCL = High;
	Delay_1us();
	I2C_Idle();

	I2C_Start();
	I2C_WriteByte( ( Slave_address + 1 ) );		// Read
	I2C_ACK_Slave();

	Temp_Data = I2C_ReadByte();
	I2C_nACK_Master();

	I2C_Stop();
	I2C_Idle();
	return Temp_Data;
}

void Macom_WriteByte(unsigned char Slave_address,unsigned char Regcommand_Address,unsigned char SendData)
{
	I2C_Idle();
	I2C_Start();
	I2C_WriteByte(Slave_address);		// Write
	I2C_ACK_Slave();

	I2C_WriteByte(Regcommand_Address);
	I2C_ACK_Slave();

	I2C_WriteByte(SendData);
	I2C_ACK_Slave();
	I2C_Stop();
	I2C_Idle();

}

unsigned int Macom_Read_Two_Byte(unsigned char Slave_address,unsigned char Regcommand_Address)
{
	xdata unsigned int MSB_Vaule = 0 ;
	xdata unsigned int LSB_Vaule = 0 ;
	xdata unsigned int Temp_Data = 0 ;

	I2C_Idle();
	I2C_Start();
	I2C_WriteByte(Slave_address);		// Write
	I2C_ACK_Slave();

	I2C_WriteByte(Regcommand_Address);
	I2C_ACK_Slave();

	Master_SCL = High;
	Delay_1us();
	I2C_Idle();

	I2C_Start();
	I2C_WriteByte( ( Slave_address + 1 ) );		// Read
	I2C_ACK_Slave();

	MSB_Vaule = I2C_ReadByte();
	I2C_MRSlave_Ack();

	LSB_Vaule = I2C_ReadByte();
	I2C_nACK_Master();

	Temp_Data = (unsigned int)( MSB_Vaule << 4 ) + LSB_Vaule ;

	I2C_Stop();
	I2C_Idle();

	return Temp_Data;
}


