//=========================================================
// src/Interrupts.c: generated by Hardware Configurator
//
// This file will be regenerated when saving a document.
// leave the sections inside the "$[...]" comment tags alone
// or they will be overwritten!!
//=========================================================

// USER INCLUDES
#include <SI_EFM8LB1_Register_Enums.h>
#include "Master_I2C_GPIO.h"
#include "Flash_MAP.h"
#include "EFM8LB_Flash_RW.h"
#include "Calibration_Struct.h"
#include <MALD37645A_SRAM.h>
#include <MATA37644A_SRAM.h>
#include "MSA_SFF8472.h"

#include <string.h>

//-----------------------------------------------------------------------------
// SMBUS0_ISR
//-----------------------------------------------------------------------------
#define  SMB_SRADD                0x20 // (SR) slave address received
                                       //    (also could be a lost
                                       //    arbitration)
#define  SMB_SRSTO                0x10 // (SR) STOP detected while SR or ST,
                                       //    or lost arbitration
#define  SMB_SRDB                 0x00 // (SR) data byte received, or
                                       //    lost arbitration
#define  SMB_STDB                 0x40 // (ST) data byte transmitted
#define  SMB_STSTO                0x50 // (ST) STOP detected during a
                                       //    transaction; bus error
//-----------------------------------------------------------------------------
// PassWord Define
//-----------------------------------------------------------------------------

#define Slave_A0   	0xA0
#define Slave_A2   	0xA2
/*
// V0100 OLD PW
#define Leve1_PS0   0xA6
#define Leve1_PS1   0x06
#define Leve1_PS2   0x4C
#define Leve1_PS3   0x6E

#define Leve2_PS0   0xE0
#define Leve2_PS1   0x55
#define Leve2_PS2   0x3E
#define Leve2_PS3   0x8A
*/

// Luxshare-ICT Password
// V0200 PW 2018/04/19
#define Leve1_PS0   0x93
#define Leve1_PS1   0x78
#define Leve1_PS2   0xCC
#define Leve1_PS3   0xAE

#define Leve2_PS0   0x3A
#define Leve2_PS1   0x18
#define Leve2_PS2   0xB6
#define Leve2_PS3   0x6F

//-----------------------------------------------------------------------------
// Page Select define
//-----------------------------------------------------------------------------
#define MALD37645    0x80
#define MATA37644    0x88
#define Calibration  0x90
#define CTLE_EM      0x91
#define Bias0_LUT    0xA0
#define Mod0_LUT     0xA8
#define ID_Debug     0xB0
#define BL_MODE      0xD0

#define Page0        0x00
#define Page1        0x01
#define Page2        0x02

//-----------------------------------------------------------------------------
// ID Debug Info table define
//-----------------------------------------------------------------------------
xdata unsigned char ID_Debug_Info[32] =
{
	'S','L','B','F','6','4','Q','2','4',     // MCU                         // 00,01,02,03,04,05,06,07,08
    'M','L','D','3','7','6','4','5','B',     // Device_Tx                   // 09,10,11,12,13,14,15,16,17
    'M','T','A','3','7','6','4','4','B',     // Device_Rx                   // 18,19,20,21,22,23,24,25,26
    0x01,0x00,0x00,0x00,0x00                 // Internal Control version    // 27,28,29.30,31
};

#define ID_Debug_Info_SIZE sizeof(ID_Debug_Info)
//-----------------------------------------------------------------------------
// External SRAM Data
//-----------------------------------------------------------------------------
xdata unsigned char Device_Address = 0 ;
xdata unsigned char MemAddress = 0 ;
xdata unsigned char I2C_Buffer[128];
xdata unsigned char I2C_old_byte_buffer = 0 ;
xdata unsigned char Table_select = 0 ;
xdata unsigned char Get_Data_Count = 0 ;
xdata unsigned char DATA_LEGHT = 0 ;
xdata unsigned char I2C_READY_FLG = 0 ;

xdata unsigned char HOSTPASSWORD = 0 ;
xdata unsigned char Update_flag = 8 ;

xdata unsigned char Password[4];
//-----------------------------------------------------------------------------
// External SRAM Data
//-----------------------------------------------------------------------------
extern xdata unsigned char SFF8472_A0[256];
extern xdata unsigned char SFF8472_A2[256];
extern xdata unsigned int CHECKSUM;
//-----------------------------------------------------------------------------
// Update SRAM Data From Flash
//-----------------------------------------------------------------------------

void Update_SRAM()
{
	switch(SFF8472_A2[127])
	{
		case Page0 :
					   Flash_ReadFunction(  FS_SFF8472_P0  , &SFF8472_A2[128] , 128 );
					   // CheckSum MSB
					   //SFF8472_A2[248] = ( CHECKSUM >> 8 ) ;
					   // CheckSum LSB
					  // SFF8472_A2[249] = CHECKSUM ;
					   // Version MSB
					   //SFF8472_A2[250] = ( MCU_FW_VERSION >> 8 ) ;
					   // Version LSB
					   //SFF8472_A2[251] = MCU_FW_VERSION ;

					   //SFF8472_A2[252] = 0xFF;
					   //SFF8472_A2[253] = 0xFF;
					   //SFF8472_A2[254] = 0xFF;
					   //SFF8472_A2[255] = 0xFF;
					   break;
		case Page1 :
					   Flash_ReadFunction( FS_SFF8472_P1 , &SFF8472_A2[128] , 128 );
					   break;
		case Page2 :
					   Flash_ReadFunction( FS_SFF8472_P2 , &SFF8472_A2[128] , 128 );
					   break;

		case MALD37645 :
						if( HOSTPASSWORD > 1 )
						{
							MALD37645A_Read_ALL();
							memcpy( &SFF8472_A2[128] , &MALD37645A_MEMORY_MAP , 128 );
						}
						else
							memset( &SFF8472_A2[128] , 0xff , 128 ) ;

						break;
		case MATA37644 :
						if( HOSTPASSWORD > 1 )
						{
							MATA37644A_Read_ALL();
							memcpy( &SFF8472_A2[128] , &MATA37644A_MEMORY_MAP , 128 );
						}
						else
							memset( &SFF8472_A2[128] , 0xff , 128 ) ;

						break;

		case Calibration :
							if( HOSTPASSWORD > 1 )
							{
								CALIB_MEMORY_MAP.FW_VERSION = MCU_FW_VERSION ;
								memcpy( &SFF8472_A2[128] , &CALIB_MEMORY_MAP , 128 );
							}
							else
								memset( &SFF8472_A2[128] , 0xff , 128 ) ;

							break;
		case CTLE_EM :
							if( HOSTPASSWORD > 1 )
							{
								Flash_ReadFunction( MSA_Option_Table , &SFF8472_A2[128] , 128 );
								memcpy( &SFF8472_A2[128] , &CTEL_Table, 16);
								memcpy( &SFF8472_A2[144] , &EMPHASIS_Table, 16);
								memcpy( &SFF8472_A2[176] , &PW_LEVE1, 4 );
							}
							else
								memset( &SFF8472_A2[128] , 0xff , 128 ) ;

							break;

		case Bias0_LUT :
						if( HOSTPASSWORD > 1 )
							Flash_ReadFunction( Bias_LUT_0 , &SFF8472_A2[128] , 128 );
						else
							memset( &SFF8472_A2[128] , 0xff , 128 ) ;

						break;

		case Mod0_LUT :
						if( HOSTPASSWORD > 1 )
							Flash_ReadFunction( Mod_LUT_0 , &SFF8472_A2[128] , 128 );
						else
							memset( &SFF8472_A2[128] , 0xff , 128 ) ;

						break;

		case ID_Debug :
					    if( HOSTPASSWORD > 1 )
					    {
					    	memcpy( &SFF8472_A2[128] , &ID_Debug_Info[0] , 32 );
					    	Flash_ReadFunction( ( MPLID_INFO + 32 ) , &SFF8472_A2[160] , 96 );
					    }
					    else
					    	memset( &SFF8472_A2[128] , 0xff , 128 ) ;

						break;
		default:
			    break;
	}
}
//-----------------------------------------------------------------------------
// Write Flash Data from SRAM
//-----------------------------------------------------------------------------
void Write_Flash()
{
	switch(SFF8472_A2[127])
	{
		case Page0 :
					if( HOSTPASSWORD > 0 )
					{
						Flash_EWFunction( FS_SFF8472_A2 , &SFF8472_A2[0]   , 96 );
						Flash_EW_NoErase( FS_SFF8472_P0 , &SFF8472_A2[128] , 128 );
					}
					break;
		case Page1 :
					if( HOSTPASSWORD > 0 )
						Flash_EWFunction( FS_SFF8472_P1 , &SFF8472_A2[128] , 128 );
					break;
		case Page2 :
					if( HOSTPASSWORD > 0 )
						Flash_EWFunction( FS_SFF8472_P2 , &SFF8472_A2[128] , 128 );
					break;

		case MALD37645 :
						if( HOSTPASSWORD > 1 )
						{
							if( MemAddress > 0xF7 )
							{
								if( SFF8472_A2[255] == 1 )
								{
									MALD37645A_MEMORY_MAP.DC_AD = SFF8472_A2[248] ;
									MALD37645A_MEMORY_MAP.DC_Data0 = SFF8472_A2[249] ;
									MALD37645A_MEMORY_MAP.DC_RW = SFF8472_A2[254];
									// Read
									if( MALD37645A_MEMORY_MAP.DC_RW == 0x01 )
										MALD37645A_MEMORY_MAP.DC_Data0 = Macom_ReadByte( MALD37645A_SA , MALD37645A_MEMORY_MAP.DC_AD );
									// Write
									else if( MALD37645A_MEMORY_MAP.DC_RW == 0x00 )
										Macom_WriteByte( MALD37645A_SA , MALD37645A_MEMORY_MAP.DC_AD , MALD37645A_MEMORY_MAP.DC_Data0 );
									SFF8472_A2[255] = 0x00 ;
								}
								else{;};
							}
							else
							{
								Flash_EWFunction( FS_MALD_37645A , &SFF8472_A2[128] , 120 );
								memcpy( &MALD37645A_MEMORY_MAP.CHIPID , &SFF8472_A2[128] , 120 );
								MALD37645A_Write_All();
							}
						}
						break;

		case MATA37644 :
						if( HOSTPASSWORD > 1 )
						{
							if( MemAddress > 0xF7 )
							{
								if( SFF8472_A2[255] == 1 )
								{
									MATA37644A_MEMORY_MAP.DC_AD = SFF8472_A2[248] ;
									MATA37644A_MEMORY_MAP.DC_Data0 = SFF8472_A2[249] ;
									MATA37644A_MEMORY_MAP.DC_RW = SFF8472_A2[254];
									// Read
									if( MATA37644A_MEMORY_MAP.DC_RW == 0x01 )
										MATA37644A_MEMORY_MAP.DC_Data0 = Macom_ReadByte( MATA37644A_SA , MATA37644A_MEMORY_MAP.DC_AD );
									// Write
									else if( MATA37644A_MEMORY_MAP.DC_RW == 0x00 )
										Macom_WriteByte( MATA37644A_SA , MATA37644A_MEMORY_MAP.DC_AD , MATA37644A_MEMORY_MAP.DC_Data0 );
									SFF8472_A2[255] = 0x00 ;
								}
								else{;};
							}
							else
							{
								Flash_EWFunction( FS_MATA_37644A , &SFF8472_A2[128] , 120 );
								memcpy( &MATA37644A_MEMORY_MAP.CHIPID , &SFF8472_A2[128] , 120 );
								MATA37644A_Write_All();
							}
						}
						break;

		case Calibration :
							if( HOSTPASSWORD > 1 )
							{
								memcpy( &SFF8472_A2[248] , &CALIB_MEMORY_MAP.CHECKSUM_V , 8 );
								Flash_EWFunction( Calibration_Table , &SFF8472_A2[128] , 120 );
								//Flash_EW_NoErase( (Calibration_Table+120) , &CALIB_MEMORY_MAP.CHECKSUM_V , 8 );
								memcpy( &CALIB_MEMORY_MAP , &SFF8472_A2[128] , 120 );
							}
							break;
		case CTLE_EM :
							if( HOSTPASSWORD > 1 )
							{
								Flash_EWFunction( MSA_Option_Table , &SFF8472_A2[128] , 128 );
								Get_Flash_CTLE_Table();
								Get_Flash_EMPHASIS_Table();
								Get_Flash_PW_LEVE1();
							}
							break;
		case Bias0_LUT :
							if( HOSTPASSWORD > 1 )
								Flash_EWFunction( Bias_LUT_0 , &SFF8472_A2[128] , 128 );
							break;
		case Mod0_LUT :
							if( HOSTPASSWORD > 1 )
								Flash_EWFunction( Mod_LUT_0 , &SFF8472_A2[128] , 128 );
							break;
		case ID_Debug :
							if( HOSTPASSWORD > 1 )
								Flash_EWFunction( MPLID_INFO , &SFF8472_A2[128] , 128 );

							break;
		case BL_MODE :

							if( MemAddress == 0x24 )
							{
								if( (I2C_Buffer[1]==0x11) && (I2C_Buffer[2]==0x22) && (I2C_Buffer[3]==0x33) &&
									(I2C_Buffer[4]==0x55) && (I2C_Buffer[5]==0x66) && (I2C_Buffer[6]==0x55)	)
								{
									FLASH_ByteWrite( BL_CS_CK , 0x01 , 0x55);
									// Write R0 and issue a software reset
									*((uint8_t SI_SEG_DATA *)0x00) = 0xA5;
									RSTSRC = RSTSRC_SWRSF__SET | RSTSRC_PORSF__SET;
								}

							}
							break;
		default:
			break;
	}
}

//-----------------------------------------------------------------------------
// SMBUS0_ISR
//-----------------------------------------------------------------------------
//
// SMBUS0 ISR Content goes here. Remember to clear flag bits:
// SMB0CN0::SI (SMBus Interrupt Flag)
//
//-----------------------------------------------------------------------------
SI_INTERRUPT (SMBUS0_ISR, SMBUS0_IRQn)
{
    unsigned int Data_count = 0;
	if (SMB0CN0_ARBLOST == 0)
	{
		switch (SMB0CN0 & 0xF0)          // Decode the SMBus status vector
		{
		         // Slave Receiver: Start+Address received
			case  SMB_SRADD:

		          SMB0CN0_STA = 0;           // Clear SMB0CN0_STA bit
		          Device_Address = SMB0DAT ;

		          if( ( Device_Address & 0xFE ) == ( Slave_A0 & 0xFE ) )
		          {
		        	  Table_select = 1 ;
		        	  if( Device_Address & 0x01 == 0x01 )
		        	  {
		        		  SMB0DAT = SFF8472_A0[MemAddress] ;
		        		  MemAddress++;
		        	  }
		        	  SMB0CN0_ACK = 1 ;
		          }
		          else if ( ( Device_Address & 0xFE ) == ( Slave_A2 & 0xFE ) )
		          {
		        	  Table_select = 2 ;
		        	  if( Device_Address & 0x01 == 0x01 )
		        	  {
		        		  if(( MemAddress >= 96) && (MemAddress < 120) )
		        		  {
		        			  if((MemAddress & 0x01) == 0)
								  I2C_old_byte_buffer = SFF8472_A2[MemAddress+1];
							  else
								  SFF8472_A2[MemAddress] = I2C_old_byte_buffer;

		        			  SMB0DAT = SFF8472_A2[MemAddress] ;
		        		  }
		        		  else if (( MemAddress >= 123) && (MemAddress < 127) )
		        			  SMB0DAT = 0xFF ;
		        		  else
		        			  SMB0DAT = SFF8472_A2[MemAddress] ;
		       			  MemAddress++;
		        	  }
		        	  SMB0CN0_ACK = 1 ;
		          }
		          else
		        	  SMB0CN0_ACK = 0;        // NACK received address

		          break;

		         // Slave Receiver: Data received
		    case  SMB_SRDB:

		    	  I2C_Buffer[Get_Data_Count] =  SMB0DAT ;
		    	  MemAddress = I2C_Buffer[0] ;
		    	  Get_Data_Count++;
		    	  SMB0CN0_ACK = 1;

		          break;

		         // Slave Receiver: Stop received while either a Slave Receiver or
		         // Slave Transmitter
		    case  SMB_SRSTO:

		       	  SMB0CN0_STO = 0;           // SMB0CN0_STO must be cleared by software when
		       	                             // a STOP is detected as a slave
		       	  if(Get_Data_Count>=2)                   // I2C Write Data in
		       	  {
		       		  DATA_LEGHT = ( Get_Data_Count-1 );
		       		  //slave address 0xA0
		       		  if( Table_select == 1 )                 // Write SFF8472 A0 table
		       		  {
		       			  if( ( HOSTPASSWORD == 1 ) || ( HOSTPASSWORD == 2 ) )    //  Check Password
		       				  I2C_READY_FLG = 1 ;
		       		  }
		       		  //slave address 0xA2
		       		  else if( Table_select == 2 )                            // Write SFF8472 A2 table
                      {
		       			  if( ( MemAddress == 127 ) && ( Get_Data_Count == 2 ) )
		       			  {
		       				  I2C_READY_FLG = 1 ;
		       				  // Page SEL
		       				  SFF8472_A2[127] = I2C_Buffer[1] ;
		       				  // update data function
		       				  //Update_SRAM();
		       				  Update_flag = 1 ;
		       			  }
		       			  else
		       	          {
		       				  // write Password to SRAM
		       				  if(( MemAddress >= 123 ) && ( MemAddress <= 126 ))
		       				  {
								  // Setting Password number
								  for( Data_count=0 ; Data_count<DATA_LEGHT ; Data_count++ )
									  Password[(MemAddress-123)+Data_count] = I2C_Buffer[Data_count+1];
								  if ( (Password[0] == PW_LEVE1[0]) && (Password[1] == PW_LEVE1[1]) && (Password[2] == PW_LEVE1[2]) && (Password[3] == PW_LEVE1[3]) )
									  HOSTPASSWORD = 1 ;
								  else if( (Password[0] == Leve2_PS0) && (Password[1] == Leve2_PS1) && (Password[2] == Leve2_PS2) && (Password[3] == Leve2_PS3) )
									  HOSTPASSWORD = 2 ;
								  else
									  HOSTPASSWORD = 0 ;
		       				  }
		       				  else if( MemAddress == 110 )
		       					  SFF8472_A2[110] = I2C_Buffer[1] ;
		       				  else if( MemAddress == 118 )
		       					  SFF8472_A2[118] = I2C_Buffer[1] ;
		       				  else
							  {
		       					  if( ( HOSTPASSWORD == 1 ) || ( HOSTPASSWORD == 2 ) )
		       					  {
		       						  I2C_READY_FLG = 1 ;
		       						  Update_flag = 2 ;
		       						  // I2C_Buffer to SRAM
		       						  for(Data_count=0;Data_count<DATA_LEGHT;Data_count++)
		       							  SFF8472_A2[(MemAddress+Data_count)] = I2C_Buffer[ (Data_count + 1 )];
		       					  }
							  }
		       	          }
                      }
		       		  else if( Table_select == 3 )
		       		  {
		       			  I2C_READY_FLG = 1 ;
		       		  }
		       	  }
		       	  Get_Data_Count = 0 ;


		          break;

		         // Slave Transmitter: Data byte transmitted
		    case  SMB_STDB:

		       	  if( SMB0CN0_ACK == 1 )
		          {
		       		  // slave address 0xA0
		       		  if(Table_select==1)
		       		  {
		       			  SMB0DAT = SFF8472_A0[MemAddress] ;
		       			  MemAddress ++ ;
		       		  }
		       		  // slave address 0xA2
		       		  else if(Table_select==2)
		       		  {
		       			  if( ( MemAddress>=96 ) && ( MemAddress < 120 ) )
						  {
							  if( (MemAddress & 0x01 ) == 0)
								  I2C_old_byte_buffer = SFF8472_A2[MemAddress+1];
							  else
								  SFF8472_A2[MemAddress] = I2C_old_byte_buffer ;

							  SMB0DAT = SFF8472_A2[MemAddress] ;
						  }
		       			  else if (( MemAddress >= 123) && (MemAddress < 127) )
		       				  SMB0DAT = 0xFF ;
		       			  else
		       				  SMB0DAT = SFF8472_A2[MemAddress] ;

		       			  MemAddress ++ ;
		       		  }

		          }
		                                       // No action required;
		                                       // one-byte transfers
		                                       // only for this example
		          break;

		         // Slave Transmitter: Arbitration lost, Stop detected
		         //
		         // This state will only be entered on a bus error condition.
		         // In normal operation, the slave is no longer sending data or has
		         // data pending when a STOP is received from the master, so the SMB0CN0_TXMODE
		         // bit is cleared and the slave goes to the SRSTO state.
		     case  SMB_STSTO:

		           SMB0CN0_STO = 0;           // SMB0CN0_STO must be cleared by software when
		                                       // a STOP is detected as a slave
		           break;

		         // Default: all other cases undefined
		     default:

		            SMB0CF &= ~0x80;           // Reset communication
		            SMB0CF |= 0x80;
		            SMB0CN0_STA = 0;
		            SMB0CN0_STO = 0;
		            SMB0CN0_ACK = 0;
		            break;
		}

	}
		   // SMB0CN0_ARBLOST = 1, Abort failed transfer
    else
	{
    	SMB0CN0_STA = 0;
		SMB0CN0_STO = 0;
		SMB0CN0_ACK = 0;
	}

	SMB0CN0_SI = 0;                     // Clear SMBus interrupt flag
}

//-----------------------------------------------------------------------------
// TIMER3_ISR
//-----------------------------------------------------------------------------
//
// TIMER3 ISR Content goes here. Remember to clear flag bits:
// TMR3CN0::TF3H (Timer # High Byte Overflow Flag)
// TMR3CN0::TF3L (Timer # Low Byte Overflow Flag)
//
//-----------------------------------------------------------------------------
SI_INTERRUPT (TIMER3_ISR, TIMER3_IRQn)
{
	SMB0CF  &= ~0x80;                   // Disable SMBus
	SMB0CF  |=  0x80;                   // Re-enable SMBus
	TMR3CN0 &= ~0x80;                   // Clear Timer3 interrupt-pending flag
}

//-----------------------------------------------------------------------------
// TIMER3_ISR
//-----------------------------------------------------------------------------
void SRMA_Flash_Function()
{
	unsigned char Data_count;
	if(Table_select==1 && I2C_READY_FLG == 1) // A0 Write
	{
		for(Data_count=0;Data_count<DATA_LEGHT;Data_count++)
			SFF8472_A0[( MemAddress + Data_count )]=I2C_Buffer[ ( Data_count + 1 ) ];

		if( HOSTPASSWORD >= 1 )
			Flash_EWFunction( FS_SFF8472_A0 , &SFF8472_A0, 256 );

		I2C_READY_FLG = 0 ;
	}
	else if(Table_select==2 && I2C_READY_FLG == 1) // A2 Write
	{
		if( Update_flag == 1 )
			Update_SRAM();
		else if( Update_flag == 2 )
			Write_Flash();

		I2C_READY_FLG = 0 ;
	}

}


