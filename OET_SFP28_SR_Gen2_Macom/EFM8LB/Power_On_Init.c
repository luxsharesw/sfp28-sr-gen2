/*
 * Power_On_Init.c
 *
 *  Created on: 2017�~3��8��
 *      Author: 100476
 */
#include <SI_EFM8LB1_Register_Enums.h>
#include <MALD37645A_SRAM.h>
#include <MATA37644A_SRAM.h>
#include "Flash_MAP.h"
#include "EFM8LB_Flash_RW.h"
#include "Calibration_Struct.h"

#include <string.h>

xdata struct CALIB_MEMORY CALIB_MEMORY_MAP;

xdata unsigned int CHECKSUM = 0;
xdata unsigned char SFF8472_A0[256];
xdata unsigned char SFF8472_A2[256];

xdata unsigned char EMPHASIS_Table[16];
xdata unsigned char CTEL_Table[16];

xdata unsigned char PW_LEVE1[4];

void CheckSum_Calculate()
{
	unsigned int TEMP_BUF;
	unsigned int IIcount;

	CHECKSUM = 0;
	for( IIcount=0 ; IIcount < Flash_LUT_Start ; IIcount++ )
	{
		TEMP_BUF = FLASH_ByteRead(IIcount);
		CHECKSUM = CHECKSUM + TEMP_BUF ;
	}
	CALIB_MEMORY_MAP.CHECKSUM_V = CHECKSUM ;
	Flash_EWFunction( Calibration_Table , &CALIB_MEMORY_MAP.VCC_SCALEM , 128 );
}

void CheckSum_Calculation()
{
	unsigned char CHECKSUM_BUFFER;
	CHECKSUM_BUFFER = FLASH_ByteRead( BL_CS_CK );

	if( CHECKSUM_BUFFER == 0x01 )
	{
		CheckSum_Calculate();
		FLASH_PageErase( BL_CS_CK , 0x55 );
	}
	else
		CHECKSUM = CALIB_MEMORY_MAP.CHECKSUM_V ;

//	SFF8472_A2[248] = CHECKSUM >> 8 ;
//	SFF8472_A2[249] = CHECKSUM ;

//	SFF8472_A2[250] = ( MCU_FW_VERSION >> 8 ) ;
//	SFF8472_A2[251] = MCU_FW_VERSION ;
}

void SFF8472_Table_GetDate()
{
	Flash_ReadFunction( FS_SFF8472_A0 , &SFF8472_A0 , 256 );
	Flash_ReadFunction( FS_SFF8472_A2 , &SFF8472_A2 , 256 );
}

void Device_Table_Get_Data()
{
	Flash_ReadFunction( FS_MALD_37645A , &MALD37645A_MEMORY_MAP.CHIPID , 128 );

	Flash_ReadFunction( FS_MATA_37644A , &MATA37644A_MEMORY_MAP.CHIPID , 128 );

	Flash_ReadFunction( Calibration_Table , &CALIB_MEMORY_MAP.VCC_SCALEM , 128 );
}

void Get_Flash_EMPHASIS_Table()
{
	Flash_ReadFunction( ( MSA_Option_Table+16 ) , &EMPHASIS_Table , 16 );
}

void Get_Flash_CTLE_Table()
{
	Flash_ReadFunction( MSA_Option_Table , &CTEL_Table , 16 );
}

void Get_Flash_PW_LEVE1()
{
	Flash_ReadFunction( ( MSA_Option_Table + 48 ) , &PW_LEVE1 , 4 );
}

void A2_SET_Value()
{
	// soft rate RS0 = 0 ; Low rate
	// HW RS0 Control
	SFF8472_A2[110] = 0x00;

	SFF8472_A2[112] = 0x00;
	SFF8472_A2[113] = 0x00;
	// MSA EQ & EM default
	SFF8472_A2[114] = 0x66;
	SFF8472_A2[115] = 0x00;

	SFF8472_A2[116] = 0x00;
	SFF8472_A2[117] = 0x00;

	// soft rate RS1 = 0 ; Low rate
	// HW RS1 Control
	SFF8472_A2[118] = 0x00;
	SFF8472_A2[119] = 0x00;
}

void PowerOn_Table_Init()
{
	SFF8472_Table_GetDate();
	A2_SET_Value();
	Device_Table_Get_Data();

	Get_Flash_CTLE_Table();
	Get_Flash_EMPHASIS_Table();
	Get_Flash_PW_LEVE1();

	CheckSum_Calculation();
}




