/*
 * Tempsensor.c
 *
 *  Created on: 2017�~3��7��
 *      Author: 100476
 */

#include <SI_EFM8LB1_Register_Enums.h>
#include "InitDevice.h"

//-----------------------------------------------------------------------------
// Global Constants
//-----------------------------------------------------------------------------
#define SCALE                     256L // Scale for temp calculations

#define TEMP_CAL_ADDRESS_LOW    0xFFD4 // Address in flash where the
                                       // temp cal low byte value is stored

#define TEMP_CAL_ADDRESS_HIGH   0xFFD5 // Address in flash where the
                                       // temp cal high byte value is stored

#define RSSI_ADC_CH      ADC0MX_ADC0MX__ADC0P8
#define VCC_ADC_CH       ADC0MX_ADC0MX__ADC0P9
#define V1V8_MON_CH      ADC0MX_ADC0MX__ADC0P5
#define VCC_Monitor      ADC0MX_ADC0MX__VDD

//#define ADC_14B      16383
//#define VREF         2.4

//#define ADC_14B      16383
//#define VREF         1.65

#define ADC_COV_100uV  1.1
#define ADC_Gain_05    2


unsigned int tempsensor_0c;
long ADC_Buffer;

// Calibration value for the temperature sensor at 0 degrees C, stored in CODE space
SI_LOCATED_VARIABLE_NO_INIT(TEMPSENSOR_0C_LOW, uint8_t const, SI_SEG_CODE, TEMP_CAL_ADDRESS_LOW);
SI_LOCATED_VARIABLE_NO_INIT(TEMPSENSOR_0C_HIGH, uint8_t const, SI_SEG_CODE, TEMP_CAL_ADDRESS_HIGH);

void Tempsonr_0C_internal_Cal()
{
	tempsensor_0c = ( (TEMPSENSOR_0C_HIGH << 8) | TEMPSENSOR_0C_LOW )*0.69;
}

unsigned int ADC_Covn(unsigned char CH , unsigned char Sampling)
{
	unsigned char SVAE_SFRPAGE;
	unsigned int Tcount;
	unsigned char IIcount;
	long ADC_Buffer = 0 ;
	unsigned int Update_Buffer;

	SVAE_SFRPAGE = SFRPAGE ;
	SFRPAGE = LEGACY_PAGE ;

	for(IIcount=0;IIcount<Sampling;IIcount++)
	{
		ADC0MX = CH;
		for(Tcount=0;Tcount<20;Tcount++);
		ADC0CN0_ADBUSY = 1 ;
		while( ADC0CN0_ADINT == 0 );

		ADC_Buffer = ADC_Buffer + ADC0 ;

		ADC0CN0_ADBUSY = 0 ;
		ADC0CN0_ADINT = 0 ;
	}

	Update_Buffer = ( ADC_Buffer / Sampling ) ;
	SFRPAGE = SVAE_SFRPAGE ;

	return Update_Buffer ;
}

unsigned int Get_ADC_Value(unsigned char CH , unsigned char Sampling)
{
	unsigned int Tcount;
	unsigned char IIcount;
	long ADC_Buffer = 0 ;
	unsigned int Update_Buffer;

	for(IIcount=0;IIcount<Sampling;IIcount++)
	{
		ADC0MX = CH;
		for(Tcount=0;Tcount<10;Tcount++);
		ADC0CN0_ADBUSY = 1 ;
		while( ADC0CN0_ADINT == 0 );

		ADC_Buffer = ADC_Buffer + ADC0 ;

		ADC0CN0_ADBUSY = 0 ;
		ADC0CN0_ADINT = 0 ;
	}

	Update_Buffer =( ADC_Buffer / Sampling )*ADC_COV_100uV*ADC_Gain_05;

	return Update_Buffer ;
}


int Get_Tempsensor()
{
	unsigned int Temp;
	int Tempsenor_Buffer;

	Temp = Get_ADC_Value( ADC0MX_ADC0MX__TEMP , 4 );
	//Temp = 5000;
	Tempsenor_Buffer = ( (int)( Temp - 7510 ) / (28.2)) ;

	Tempsenor_Buffer = Tempsenor_Buffer<<8;

	return Tempsenor_Buffer;
}

unsigned int VCC_GetValue()
{
	unsigned int VCC_Value;

//	VCC_Value = ADC_Covn( VCC_Monitor , 4 );

	VCC_Value = Get_ADC_Value( VCC_Monitor , 4 );

	return VCC_Value;
}

unsigned int V18_GetValue()
{
	unsigned int V18_Value;

	V18_Value = ADC_Covn( V1V8_MON_CH , 4 );
	V18_Value = ( V18_Value * ADC_COV_100uV );

	return V18_Value;
}

unsigned int RSSI_GET_Value()
{
	unsigned int RSSI_Value;

	RSSI_Value = Get_ADC_Value( RSSI_ADC_CH , 4 ); // ADC_Covn( RSSI_ADC_CH , 4 );

	return RSSI_Value;
}






